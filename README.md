# Program
| Week | Date     | Task                                                     | Done |
|------|----------|----------------------------------------------------------|------|
| 1    | 01/02/16 | -                                                        |      |
| 2    | 08/02/16 | Find some modbus traffic traces                          | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
| 3    | 15/02/16 | Create a modbus program to receive the incoming traffic  | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
|      |          | Setting up some basic IDS to sort out the traffic        | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
| 4    | 22/02/16 | Find a program to generate some scan attack              | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
|      |          | IDS live trainer                                         | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
| 5    | 29/02/16 | Find some more attacks                                   | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png) |
|      |          | Upgrade the program to inject the attack into the traces | ![](https://cdn4.iconfinder.com/data/icons/sketchdock-ecommerce-icons/ok-green.png)|
| 6    | 07/03/16 |||
| 7    | 14/03/16 |||
| 8    | 21/03/16 |||
| -    | 28/03/16 | Write more of the master thesis                          | ![](http://jasig.github.io/cas/images/indev.png)|
|      |          | Generate some traffic traces from simulation             | ![](http://jasig.github.io/cas/images/indev.png)|
| -    | 04/04/16 | Write more of the master thesis                          | ![](http://jasig.github.io/cas/images/delete.png)|
| 9    | 11/04/16 | Implement the new attacks in the generator               | ![](http://jasig.github.io/cas/images/delete.png)|
|      |          | Debug and upgrade the simulation                         | ![](http://jasig.github.io/cas/images/delete.png)|
| 10   | 18/04/16 |||
| 11   | 25/04/16 |||
| 12   | 02/05/16 |||
| 13   | 09/05/16 |||
| 14   | 16/05/16 |||
| 15   | 23/05/16 |||
| 16   | 30/05/16 | Remise ||
