# Intrusion Detection System

### Requirements
The IDS requires [Slog](https://github.com/kala13x/slog) for the logging system.


### Command
The IDS we have implemented uses two whitelist here the command to run it
```
./sniffer -f <training_trace> -m <monitoring_trace> -p <modbus_port> -s <number_slave> -o <file_index>
```
The `training_trace` is used to build the whitelists and the `monitoring_trace`
will be the trace on which on the IDS will be run on. `number_slave` is used
because the simulation run on a local on consecutive port number, if it the
`modbus_port` port is the same for all PLCs ( ex:502 ) you can set it to 0.
Otherwise the ports numbers must be consecutive and the `modbus_port` will
be the smallest one. The `file_index` is the name of the file were the ids will
output the index of a malicious packet in the trace.

There is a script in `/src/CLI/utils/truncate.py` that can be used to truncate trace.
This is useful to generate different `training_trace`
```
python truncate.py <original_trace> <name_truncate_file> <nbr_packet>
```
The `nbr_packet` defines from where in the trace to truncate.

## Snort
First you need to install [Snort](https://www.snort.org). Snort is a signature
based IDS which uses rules to describe signature of attack. When installing
snort there should be a file in ```\etc\snort\snort.conf``` and a ```\etc\snort\rules\```
directory containing the rule files. There is a ```snort.conf``` file in this
repository that you can use. To detect MODBUS oriented attack and some scan attack,
you can add the file ```scan.rules``` and ```modbus_1_2.rules``` to the rules
###### Note:
scan.rules already exist so you need to overwrite it

### Command
When running this command, the ids will output a file containing the index and
sid of the rules that the packet triggered. You can run the second command to
extract the index.
```
sudo snort -r <trace> -c <snort_conf_file> -A console:test -k none > <output_file>
```

```
python get_index.py <output_file> <index_file>
```
