/*
   sniffer.c

   Example packet sniffer using the libpcap packet capture library available
   from http://www.tcpdump.org.

   ------------------------------------------

   Copyright © 2012 [Vic Hargrave - http://vichargrave.com]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <slog.h>
//#include <libexplain/fgets.h>
#include <signal.h>

#include "modbus_format.h"
#include "funmap.h"
#include "flows.h"
#define  DO_NOTHING 0
#define  ALLOW 1
#define  ALERT 2


pcap_t* pd;
int linkhdrlen;
flow_record *flow_hash_table = NULL;
fun_record *fun_hash_table = NULL;
int learning_phase = 0;
int port,nb_server = 0;
int packet_index = 0;
FILE *output_index;

pcap_t* open_pcap_socket(char* device, const char* bpfstr)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* pd;
    uint32_t  srcip, netmask;
    struct bpf_program  bpf;

    // If no network interface (device) is specfied, get the first one.
    if (!*device && !(device = pcap_lookupdev(errbuf)))
    {
        printf("pcap_lookupdev(): %s\n", errbuf);
        return NULL;
    }

    // Open the device for live capture, as opposed to reading a packet
    // capture file.
    if ((pd = pcap_open_live(device, BUFSIZ, 1, 0, errbuf)) == NULL)
    {
        printf("pcap_open_live(): %s\n", errbuf);
        return NULL;
    }

    // Get network device source IP address and netmask.
    if (pcap_lookupnet(device, &srcip, &netmask, errbuf) < 0)
    {
        printf("pcap_lookupnet: %s\n", errbuf);
        return NULL;
    }

    // Convert the packet filter epxression into a packet
    // filter binary.
    if (pcap_compile(pd, &bpf, (char*)bpfstr, 0, netmask))
    {
        printf("pcap_compile(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    // Assign the packet filter to the given libpcap socket.
    if (pcap_setfilter(pd, &bpf) < 0)
    {
        printf("pcap_setfilter(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    return pd;
}

void capture_loop(pcap_t* pd, int packets, pcap_handler func)
{
    int linktype;

    // Determine the datalink layer type.
    if ((linktype = pcap_datalink(pd)) < 0)
    {
        printf("pcap_datalink(): %s\n", pcap_geterr(pd));
        return;
    }

    // Set the datalink layer header size.
    switch (linktype)
    {
    case DLT_NULL:
        linkhdrlen = 4;
        printf("No ethernet header");
        break;

    case DLT_EN10MB:
        linkhdrlen = 14;
        break;

    case DLT_SLIP:
    case DLT_PPP:
        linkhdrlen = 24;
        break;

    default:
        printf("Unsupported datalink (%d)\n", linktype);
        return;
    }

    // Start capturing packets.
    if (pcap_loop(pd, packets, func, 0) < 0)
        printf("pcap_loop failed: %s\n", pcap_geterr(pd));
}

void display_hash_table(flow_record** hash_table){
     flow_record* current,*tmp;
     HASH_ITER(hh,*hash_table,current,tmp){
        display_flow(current);
     }
}
int checkFlow(char* sourceIp,u_int sourcePort,char* destIp,u_int destPort,int proto){
    //Bidirectional flow
    char *addr_s,*addr_d;
    u_int sp,dp;

    int cmp = strncmp(sourceIp,destIp,16);
    if(cmp < 0){
        addr_s = sourceIp;
        addr_d = destIp;
        sp = sourcePort;
        dp = destPort;
    } else if (cmp > 0) {
        addr_s = destIp;
        addr_d = sourceIp;
        sp = destPort;
        dp = sourcePort;
    } else {
         addr_s = sourceIp;
         addr_d = destIp;
         if(destPort < sourcePort){
             sp = sourcePort;
             dp = destPort;
         }else{
              dp = sourcePort;
              sp = destPort;
         }
    }

    flow_record flow;
    memset(&flow,0,sizeof(flow_record));

    flow.key.srcIp = addr_s;
    flow.key.destIp = addr_d;
    flow.key.srcPort = sp;
    flow.key.destPort = dp;
    flow.key.ipProto = proto;


    if(learning_phase){
        flow_record* new_flow;
        int num_item;
        flow_record* already = legal_flow(&flow_hash_table,&flow);
        if(already == NULL){
            if((new_flow = create_flow_record(addr_s,addr_d,sp,dp,proto))){
                printf("[Creating flow] (Prot:%d) %s:%d<>%s:%d\n",
                        new_flow->key.ipProto,new_flow->key.srcIp,new_flow->key.srcPort,
                        new_flow->key.destIp,new_flow->key.destPort);
                add_flow(&flow_hash_table,new_flow);
                num_item = HASH_COUNT(flow_hash_table);
                printf("Number of flows:%u\n",num_item);
                return DO_NOTHING;
            } else {
                return -1;
            }
        } else
            return DO_NOTHING;
    } else {
        flow_record* result = legal_flow(&flow_hash_table,&flow);
        packet_index +=1;
        if(result==NULL)
            return ALERT;
        else
            return ALLOW;
    }
}

int check_fun(char* ip,u_int port,uint8_t funcode,uint16_t len)
{
    if(learning_phase){
        fun_record fun;
        fun_record* new_fun;
        memset(&fun,0,sizeof(fun_record));
        fun.key.destIp = ip;
        fun.key.destPort = port;
        fun.key.funcode = funcode;
        fun.key.length = len;
        int num_item;
        fun_record* already = legal_fun(&fun_hash_table,&fun);
        if(already == NULL){
            if((new_fun = create_fun_record(ip,port,funcode,len))){
              printf("[Creating item]  %s:%d -> fun:%d len:%d\n",new_fun->key.destIp,
                      new_fun->key.destPort,new_fun->key.funcode,new_fun->key.length);
              add_fun_record(&fun_hash_table,new_fun);
              num_item = HASH_COUNT(fun_hash_table);
              printf("Number of item:%d\n",num_item);
              return DO_NOTHING;
            } else {
              return -1;
            }
        } else {
             return DO_NOTHING;
        }
    } else {
        fun_record fun;
        memset(&fun,0,sizeof(fun_record));
        fun.key.destIp = ip;
        fun.key.destPort = port;
        fun.key.funcode = funcode;
        fun.key.length = len;
        fun_record *result = legal_fun(&fun_hash_table,&fun);
        if(result == NULL){
            return ALERT;
        } else {
            return ALLOW;
        }
    }

}

void tcp_handler(const u_char *packet,char *sourceIp,char *destIp,const struct pcap_pkthdr *pkthdr){

    u_int sourcePort, destPort;
    struct tcphdr* tcpHeader;
    u_char *data;
    modbus_packet* payload;
    //modbus_res* response;
    int dataLength = 0;

    tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
    sourcePort = ntohs(tcpHeader->source);
    destPort = ntohs(tcpHeader->dest);

    data = (u_char*)(packet + sizeof(struct ether_header) + sizeof(struct ip) + 4*tcpHeader->doff);
    dataLength = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + 4*tcpHeader->doff);
    if((destPort >= port && destPort <= port+nb_server)
            && (tcpHeader->psh !=0 && tcpHeader->ack !=0))
        payload = (modbus_packet*) data;

    int ret = checkFlow(sourceIp,sourcePort,destIp,destPort,IPPROTO_TCP);
    switch(ret){
        case DO_NOTHING:
            if((destPort >= port && destPort <= port+nb_server)
                    &&(tcpHeader->psh !=0 && tcpHeader->ack !=0))
                ret = check_fun(destIp,destPort,(payload->func_code),htons(payload->msg_len));
            break;
        case ALERT:
            fprintf(output_index,"%d\n",packet_index);
            slog(1,SLOG_NONE,"[%s] [IP/TCP]%s:%d -> %s:%d\n",strclr(CLR_RED,"ALERT"),sourceIp,sourcePort,destIp,destPort);
            break;
        case ALLOW:
            if((destPort >= port && destPort <= port+nb_server)
                    &&(tcpHeader->psh !=0 && tcpHeader->ack !=0))
                ret = check_fun(destIp, destPort, (payload->func_code), htons(payload->msg_len));
                if(ret==ALERT){
                    fprintf(output_index,"%d\n",packet_index);
                    slog(1,SLOG_NONE,"[%s] [Modbus] %s:%d -> %s:%d Illegal function code %d or length of PDU %d",
                            strclr(CLR_RED,"ALERT"),sourceIp,sourcePort,destIp,destPort,
                            payload->func_code,htons(payload->msg_len));
                    }
            break;
        default:
            break;
    }
}

void udp_handler(const u_char *packet,char *sourceIp,char *destIp){

    struct udphdr* udphdr;
    u_int sourcePort, destPort;
    udphdr = (struct udphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
    sourcePort = ntohs(udphdr->source);
    destPort = ntohs(udphdr->dest);
    int ret = checkFlow(sourceIp,sourcePort,destIp,destPort,IPPROTO_UDP);
    switch(ret){
         case DO_NOTHING:
             break;
         case ALLOW:
             break;
         case ALERT:
             slog(1,SLOG_NONE,"[%s] [IP/UDP]%s:%d -> %s:%d\n",strclr(CLR_RED,"ALERT"),sourceIp,sourcePort,destIp,destPort);
         default:
             break;
    }
}

void icmp_handler(const u_char *packet,char *sourceIp,char *destIp){

    struct icmphdr* icmphdr;
    unsigned short id, seq;

    icmphdr = (struct icmphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
    memcpy(&id, (u_char*)icmphdr+4, 2);
    memcpy(&seq, (u_char*)icmphdr+6, 2);
    slog(0,SLOG_NONE,"[%s] [ICMP] %s -> %s\tType:%d Code:%d ID:%d Seq:%d\n",
            strclr(CLR_GREEN,"ALLOW"),
            sourceIp,destIp, icmphdr->type,
            icmphdr->code,ntohs(id),ntohs(seq));

}

void offline_learning(pcap_t *handle){
    //Read Pcap file
    const struct pcap_pkthdr pkthdr;
    u_char *packet;

    struct ether_header* ethernetHeader;
    struct ip* ipHeader;
    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];


    while((packet=pcap_next(handle,&pkthdr))){
        ethernetHeader = (struct ether_header*)packet;
        if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
            ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
            inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
            inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);
            switch(ipHeader->ip_p){
                case IPPROTO_TCP:
                    tcp_handler(packet,sourceIp,destIp,&pkthdr);
                    break;
                case IPPROTO_UDP:
                    udp_handler(packet,sourceIp,destIp);
                    break;
                case IPPROTO_ICMP:
                    icmp_handler(packet,sourceIp,destIp);
                    break;
            }
        }
    }
}


void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) {
    struct ether_header* ethernetHeader;
    struct ip* ipHeader;
    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];

    ethernetHeader = (struct ether_header*)packet;
    if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
        ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
        inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

        switch(ipHeader->ip_p){
            case IPPROTO_TCP:
                tcp_handler(packet,sourceIp,destIp,pkthdr);
                break;
            case IPPROTO_UDP:
                udp_handler(packet,sourceIp,destIp);
                break;
            case IPPROTO_ICMP:
                icmp_handler(packet,sourceIp,destIp);
                break;
        }

    }
}




void create_hash(FILE* file,pcap_t *handle){

    //Parse file
    char line[256];
    u_int destPort;
    u_int srcPort;
    u_int8_t ipProto;
    char* dest;
    char* src;

    while(fgets(line,sizeof(line),file)){
        parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
        checkFlow(src,srcPort,dest,destPort,ipProto);
    }
    learning_phase = 0;
    offline_learning(handle);
}



void bailout(int signo)
{
    struct pcap_stat stats;

    if (pcap_stats(pd, &stats) >= 0)
    {
        printf("\n%d packets received\n", stats.ps_recv);
        printf("%d packets dropped\n\n", stats.ps_drop);
    }
    clear_hash_record(&flow_hash_table);
    clear_hash_fun_record(&fun_hash_table);
    pcap_close(pd);
    exit(0);
}

void clear(){
    clear_hash_record(&flow_hash_table);
    clear_hash_fun_record(&fun_hash_table);
}

void finish_learning(int signo){
     printf("\nFinished learning phase\n");
     learning_phase = 0;
}

int main(int argc, char **argv)
{
    char interface[256] = "", bpfstr[256] = "";
    int packets = 0, c, i;
    unsigned int time = 0;

    char* filename = NULL;
    char* rulefile = NULL;
    char* tracefile = NULL;
    char* index_file = NULL;
    FILE *fp = NULL;
    // Get the command line options, if any
    while ((c = getopt (argc, argv, "hi:n:f:t:p:s:r:m:o:")) != -1)
    {
        switch (c)
        {
        case 'h':
            printf("usage of %s\n",argv[0]);
            printf("\t[-h]\t\t -Display usage manual\n");
            printf("\t[-i]<interface>\t -Specify the interface to listen on\n");
            printf("\t[-f]<file>\t -trace to read for offline training\n");
            printf("\t[-n]<number>\t -Number of packet to sniff \n");
            printf("\t[-t]<second>\t -Duration of the learning phase in second\n");
            printf("\t[-p]<port>\t -Specify the modbus port\n");
            printf("\t[-s]<nb>\t -Number of slave\n");
            printf("\t[-r]<filename>\t -File containing the rule\n");
            printf("\t[-m]<file>\t -File to monitor\n");
            printf("\t[-o]<file>\t -File to output packet index\n");
            exit(0);
            break;
        case 'i':
            strcpy(interface, optarg);
            break;
        case 'n':
            packets = atoi(optarg);
            break;
        case 'f':
            filename = optarg;
            learning_phase = 1;
            break;
        case 't':
            time = atoi(optarg);
            learning_phase = 1;
            break;
        case 'p':
            port = atoi(optarg);
            break;
        case 's':
            nb_server = atoi(optarg);
            break;
        case 'r':
            rulefile = optarg;
            learning_phase = 1;
            break;
        case 'm':
            tracefile = optarg;
            break;
        case 'o':
            index_file = optarg;
            output_index = fopen(index_file,"w+");
            if(output_index == NULL)
                exit(EXIT_FAILURE);
            break;
        }
    }

    /*
        slog_init - Initialise slog
        First argument is log filename
        Second argument is config file
        Third argument is max log level on console
        Fourth is max log level on file
        Fitfh is thread safe flag
    */
    slog_init("logs/trace_log.txt","log.cfg",2,2,1);

    if(filename != NULL){
        pcap_t *handle;
        pcap_t *trace_handle;
        char errbuf[PCAP_ERRBUF_SIZE];
        handle = pcap_open_offline(filename,errbuf);
        if (handle == NULL){
            printf("Cannot open pcap file: %s",errbuf);
            return EXIT_FAILURE;
        } else {
            if(rulefile != NULL){
                fp = fopen(rulefile,"r");
                if(fp == NULL)
                    exit(EXIT_FAILURE);
                else{
                    create_hash(fp,handle);
                    fclose(fp);
                    clear();
                    exit(0);
                }
            } else {
                    offline_learning(handle);
                    learning_phase = 0;
                    printf("\nFinished learning phase\n");
                    if(tracefile != NULL){
                        trace_handle = pcap_open_offline(tracefile,errbuf);
                        if (trace_handle == NULL){
                            printf("Cannot open pcap file: %s",errbuf);
                            return EXIT_FAILURE;
                        } else {
                            offline_learning(trace_handle);
                            clear();
                            exit(0);
                        }
                    }
            }
        }
    }
    fclose(output_index);

    if(time !=0){
        signal(SIGALRM,finish_learning);
        alarm(time);
    }

    // Get the packet capture filter expression, if any.
    for (i = optind; i < argc; i++)
    {
        strcat(bpfstr, argv[i]);
        strcat(bpfstr, " ");
    }

    // Open libpcap, set the program termination signals then start
    // processing packets.
    if ((pd = open_pcap_socket(interface, bpfstr)))
    {
        signal(SIGINT, bailout);
        signal(SIGTERM, bailout);
        signal(SIGQUIT, bailout);
        capture_loop(pd, packets, (pcap_handler)packetHandler);
        bailout(0);
    }
    exit(0);
}
