#include "funmap.h"
#include <string.h>

void clear_hash_fun_record(fun_record** hash_f)
{
     fun_record *current,*tmp;
     HASH_ITER(hh,*hash_f,current,tmp){
          HASH_DEL(*hash_f,current);
          free(current);
     }
}

fun_record* create_fun_record(char *destIp,u_int destPort,uint8_t funcode,uint16_t length){
    fun_record* fun;
    fun = (fun_record*) malloc (sizeof(fun_record));
    if(fun!=NULL){
        memset(fun,0,sizeof(fun_record));
        (fun->key).destIp = (char *)malloc(16*sizeof(char));
        if((fun->key).destIp){
            (fun->key).destIp = destIp;
        } else {
            printf("[create_flow]-Could not allocate memory for flow");
            free(fun);
            fun=NULL;
            return NULL;
        }
        fun->key.destPort = destPort;
        fun->key.funcode = funcode;
        fun->key.length = length;
        return fun;
    } else {
        printf("[create_flow]-Could not allocate memory for flow");
        return NULL;
    }
}

void add_fun_record(fun_record** hash, fun_record *record){
    HASH_ADD(hh,*hash,key,sizeof(fun_record_key),record);
}

fun_record* legal_fun(fun_record** hash,fun_record *record){
    fun_record* fun;
    HASH_FIND(hh,*hash, &(record->key),sizeof(fun_record_key),fun);
    return fun;
}

void line_to_funmap(char line[],char** destIp,u_int* destPort,uint8_t* funcode,uint16_t* length){
    char *word;
    word=strtok(line,"\t");
    //1st word
    *destIp = word;
    word = strtok(NULL,"\t");
    //2nd word
    if(strncmp(word,ANY,3)==0){
        *destPort=ANYPORT;
    } else{
        *destPort = atoi(word);
    }
    word = strtok(NULL,"\t");
    //3rd word
    *funcode = word;
    word = strtok(NULL,"\t");
    *length = word;
}



