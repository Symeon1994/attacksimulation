#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "../uthash.h"
#include "../flows.h"


#define hashsize(n) (1U << (n))
#define hashmask(n) (hashsize(n) - 1)

#define mix(a,b,c) \
{ \
    a -= b; a -= c; a ^= (c >> 13); \
    b -= c; b -= a; b ^= (a << 8); \
    c -= a; c -= b; c ^= (b >> 13); \
    a -= b; a -= c; a ^= (c >> 12); \
    b -= c; b -= a; b ^= (a << 16); \
    c -= a; c -= b; c ^= (b >> 5); \
    a -= b; a -= c; a ^= (c >> 3); \
    b -= c; b -= a; b ^= (a << 10); \
    c -= a; c -= b; c ^= (b >> 15); \
}

#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
      || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                               +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

unsigned int sfh_hash (const char* data, int len) {
    unsigned int hash = len, tmp;
    int rem;

    if (len <= 0 || data == NULL)
            return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (;len > 0; len--) {
        hash  += get16bits (data);
        tmp    = (get16bits (data+2) << 11) ^ hash;
        hash   = (hash << 16) ^ tmp;
        data  += 2*sizeof (uint16_t);
        hash  += hash >> 11;

    }

    /* Handle end cases */
    switch (rem) {
        case 3: hash += get16bits (data);
                hash ^= hash << 16;
                hash ^= ((signed char)data[sizeof (uint16_t)]) << 18;
                hash += hash >> 11;
                break;
        case 2: hash += get16bits (data);
                hash ^= hash << 11;
                hash += hash >> 17;
                break;
        case 1: hash += (signed char)*data;
                hash ^= hash << 10;
                hash += hash >> 1;

    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}

unsigned int jen_hash(unsigned char *k, unsigned length, unsigned initval)
{
    unsigned a, b;
    unsigned int c = initval;
    unsigned len = length;

    a = b = 0x9e3779b9;

    while (len >= 12)
    {
        a += (k[0] + ((unsigned)k[1] << 8) + ((unsigned)k[2] << 16) + ((unsigned)k[3] << 24));
        b += (k[4] + ((unsigned)k[5] << 8) + ((unsigned)k[6] << 16) + ((unsigned)k[7] << 24));
        c += (k[8] + ((unsigned)k[9] << 8) + ((unsigned)k[10] << 16) + ((unsigned)k[11] << 24));

        mix(a, b, c);

        k += 12;
        len -= 12;

    }

    c += length;

    switch (len)
    {
        case 11: c += ((unsigned)k[10] << 24);
        case 10: c += ((unsigned)k[9] << 16);
        case 9: c += ((unsigned)k[8] << 8);
                /* First byte of c reserved for length */
        case 8: b += ((unsigned)k[7] << 24);
        case 7: b += ((unsigned)k[6] << 16);
        case 6: b += ((unsigned)k[5] << 8);
        case 5: b += k[4];
        case 4: a += ((unsigned)k[3] << 24);
        case 3: a += ((unsigned)k[2] << 16);
        case 2: a += ((unsigned)k[1] << 8);
        case 1: a += k[0];

    }

    mix(a, b, c);

    return c;

}

unsigned int oat_hash(void *key, int len)
{
    unsigned char *p = key;
    unsigned int h = 0;
    int i;

    for (i = 0; i < len; i++)
    {
       h += p[i];
       h += (h << 10);
       h ^= (h >> 6);
    }

    h += (h << 3);
    h ^= (h >> 11);
    h += (h << 15);

    return h;

}

unsigned int ber_hash(void *key,int len)
{
    unsigned int h = 0;
    unsigned char *p = key;
    int i;

    for (i = 0;i<len;i++)
        h = ((h << 5) + h) + p[i];
    return h;
}

unsigned int sax_hash(void *key, int len)
{
    unsigned char *p = key;
    unsigned int h = 0;
    int i;

    for (i = 0; i < len; i++)
    {
       h ^= (h << 5) + (h >> 2) + p[i];
    }
    return h;
}

unsigned int fnv_hash(void *key,int len){
    unsigned char *p = key;
    unsigned h = 2166136261;
    int i;
    for(i=0;i < len;i++){
        h = (h*16777619) ^ p[i];
    }
    return h;
}

int main(int argc,const char* argv[]){
    FILE *file;
    char line[256];
    int len = atoi(argv[1]);
    unsigned int  buffer_sax[len];
    unsigned int  buffer_fnv[len];
    unsigned int  buffer_ber[len];
    unsigned int  buffer_oat[len];
    unsigned int  buffer_jen[len];
    unsigned int  buffer_sfh[len];

    for(int i=0;i<len;i++){
        buffer_sax[i] = 0;
        buffer_fnv[i] = 0;
        buffer_ber[i] = 0;
        buffer_oat[i] = 0;
        buffer_jen[i] = 0;
        buffer_sfh[i] = 0;
    }

    flow_record* new_flow = NULL;
    u_int destPort;
    u_int srcPort;
    u_int8_t ipProto;
    char* dest;
    char* src;
    int num_item;
    int nb_line = 0;
    file = fopen("ip.dataset","r");
    if(file == NULL)
        exit(-1);

    while(fgets(line,sizeof(line),file)!=NULL){
        parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
        if((new_flow = create_flow_record(src,dest,srcPort,destPort,ipProto))){
            buffer_sax[(sax_hash(&(new_flow->key),sizeof(flow_key)))%len] +=1;
            buffer_fnv[(fnv_hash(&(new_flow->key),sizeof(flow_key)))%len] +=1;
            buffer_ber[(ber_hash(&(new_flow->key),sizeof(flow_key)))%len] +=1;
            buffer_oat[(oat_hash(&(new_flow->key),sizeof(flow_key)))%len] +=1;
            buffer_jen[(jen_hash((unsigned char*)&(new_flow->key),sizeof(flow_key),0))%len] +=1;
            buffer_sfh[(sfh_hash((const char*)&(new_flow->key),sizeof(flow_key)))%len] +=1;
            nb_line++;
        } else {
            return EXIT_FAILURE;
        }
    }
    free(new_flow);
    int nb_collision_sax = 0;
    int nb_collision_fnv = 0;
    int nb_collision_ber = 0;
    int nb_collision_oat = 0;
    int nb_collision_jen = 0;
    int nb_collision_sfh = 0;

    for(int i=0; i<len;i++){
        if(buffer_sax[i] > 1){
            nb_collision_sax += 1;
        }
        if(buffer_fnv[i] > 1){
             nb_collision_fnv += 1;
        }
        if(buffer_ber[i] > 1){
            nb_collision_ber += 1;
        }
        if(buffer_oat[i] > 1){
            nb_collision_oat += 1;
        }
        if(buffer_jen[i] > 1){
             nb_collision_jen += 1;
        }
        if(buffer_sfh[i] > 1){
            nb_collision_sfh += 1;
        }

    }
    printf("\n%d\t%d\t%d\t%d\t%d\t%d\t%d",
            nb_line,nb_collision_sax,nb_collision_fnv,
            nb_collision_ber,nb_collision_oat,nb_collision_jen,
            nb_collision_sfh);
}
