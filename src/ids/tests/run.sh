#!/bin/bash

echo Running jenkins function
gcc -DHASH_FUNCTION=HASH_JEN -DHASH_EMIT_KEYS=3 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 3>hash_keys
    ./keystat hash_keys>>results/result_jen.txt
done

echo Running bernstein function
gcc -DHASH_FUNCTION=HASH_BER -DHASH_EMIT_KEYS=4 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 4>hash_keys
    ./keystat hash_keys>>results/result_ber.txt
done

echo Running shift-add-xor function
gcc -DHASH_FUNCTION=HASH_SAX -DHASH_EMIT_KEYS=5 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 5>hash_keys
    ./keystat hash_keys>>results/result_sax.txt
done

echo Running one-at-a-time function
gcc -DHASH_FUNCTION=HASH_OAT -DHASH_EMIT_KEYS=6 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 6>hash_keys
    ./keystat hash_keys>>results/result_oat.txt
done

echo Running fowler-noll-vo function
gcc -DHASH_FUNCTION=HASH_FNV -DHASH_EMIT_KEYS=7 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 7>hash_keys
    ./keystat hash_keys>>results/result_fnv.txt
done


gcc -DHASH_FUNCTION=HASH_SFH -DHASH_EMIT_KEYS=8 ../flows.c hash_function.c -o hash_function
for i in {0..10}
do
    ./hash_function 8>hash_keys
    ./keystat hash_keys>>results/result_sfh.txt
done

echo finish
