#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "../uthash.h"
#include "../flows.h"


int main(int argc, const char* argv[])
{
    FILE *file;
    int j=0;
    char line[256];
    flow_record* new_flow=NULL;
    flow_record* hash_table;
    u_int destPort;
    u_int srcPort;
    u_int8_t ipProto;
    char* dest;
    char* src;
    int num_item;
    file = fopen("ip.dataset","r");
    if(file == NULL)
        exit(-1);
    while(fgets(line,sizeof(line),file)!=NULL){
        parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
        if((new_flow = create_flow_record(src,dest,srcPort,destPort,ipProto))){
            /*printf("[Creating flow] (Prot:%d) %s:%d->%s:%d\n",
                    new_flow->key.ipProto,new_flow->key.srcIp,new_flow->key.srcPort,
                    new_flow->key.destIp,new_flow->key.destPort);*/
            add_flow(&hash_table,new_flow);
            //num_item = HASH_COUNT(hash_table);
        } else {
             return EXIT_FAILURE;
        }
    }

    num_item = HASH_COUNT(hash_table);

    fseek(file,0L,SEEK_SET);
    flow_record flow;

    while(fgets(line,sizeof(line),file) !=NULL){
         parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
         memset(&flow,0,sizeof(flow_record));
         flow.key.srcIp = src;
         flow.key.destIp = dest;
         flow.key.srcPort = srcPort;
         flow.key.destPort = destPort;
         flow.key.ipProto = ipProto;
         flow_record* already = legal_flow(&hash_table,&flow);
         if(already==NULL)
            printf("failed to find: %s:%d -> %s:%d (%d)\n",src,srcPort,dest,destPort,ipProto);
         else
            j++;
    }
    fclose(file);
    printf("Count:%d\t Found:%d\n",num_item,j);
    return EXIT_SUCCESS;
}
