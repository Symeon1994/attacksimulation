#include "flows.h"

int main(int argc,char *argv[]){
    //Read Pcap file
    struct pcap_pkthdr header;
    const u_char *packet;
    pcap_t *handle;
    char errbuf[PCAP_ERRBUF_SIZE];
    handle = pcap_open_offline(argv[1],errbuf);

    struct ether_header* ethernetHeader;
    struct ip* ipHeader;
    struct tcphdr* tcpHeader;
    struct udphdr* udphdr;
    //struct icmphdr* icmphdr;
    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];
    u_int sourcePort, destiPort;
    //unsigned short id, seq;

    char const *filename =argv[2];
    FILE* file = fopen(filename,"w+");
    //Flow and hash table
    tuple *flow=NULL;
    tuple *hash=NULL;

    if (handle == NULL){
        printf("Cannot open pcap file: %s",errbuf);
        return EXIT_FAILURE;
    }

    while((packet=pcap_next(handle,&header))){
        ethernetHeader = (struct ether_header*)packet;
        if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
            ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
            inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
            inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

            if (ipHeader->ip_p == IPPROTO_TCP) {
                tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
                sourcePort = ntohs(tcpHeader->source);
                destiPort = ntohs(tcpHeader->dest);
                tuple* result = allowed_flow(compute_key(sourceIp,destIp,sourcePort,destiPort,IPPROTO_TCP),hash);
                if(result==NULL){
                    fprintf(file,"%s\t%d\t%s\t%d\t%d\n",sourceIp,sourcePort,destIp,destiPort,IPPROTO_TCP);
                    if((flow = create_flow(sourceIp,destIp,sourcePort,destiPort,IPPROTO_TCP))>0){
                        addFlow(flow,&hash);
                    }else{
                        clearHash(&hash);
                        return EXIT_FAILURE;
                    }
                }
            }

            else if(ipHeader->ip_p == IPPROTO_UDP){
                udphdr = (struct udphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
                sourcePort = ntohs(udphdr->source);
                destiPort = ntohs(udphdr->dest);
                tuple* result = allowed_flow(compute_key(sourceIp,destIp,sourcePort,destiPort,IPPROTO_TCP),hash);
                if(result==NULL){
                    fprintf(file,"%s\t%d\t%s\t%d\t%d\n",sourceIp,sourcePort,destIp,destiPort,IPPROTO_UDP);
                    if((flow = create_flow(sourceIp,destIp,sourcePort,destiPort,IPPROTO_TCP))>0){
                        addFlow(flow,&hash);
                    }else{
                        clearHash(&hash);
                        return EXIT_FAILURE;
                    }
                }

            }
            /*else if(ipHeader->ip_p == IPPROTO_ICMP){
                icmphdr = (struct icmphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
                memcpy(&id, (u_char*)icmphdr+4, 2);
                memcpy(&seq, (u_char*)icmphdr+6, 2);
                printf("[ICMP] %s -> %s\tType:%d Code:%d ID:%d Seq:%d\n",
                        sourceIp,destIp, icmphdr->type,
                        icmphdr->code,ntohs(id),ntohs(seq));
            }*/
        }
    }

    pcap_close(handle);
    fclose(file);
    file = fopen(argv[2],"r");

    //Parse file
    char line[256];
    u_int destPort;
    u_int srcPort;
    u_int8_t ipProto;
    char* dest;
    char* src;
    while(fgets(line,sizeof(line),file)){
        parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
        if((flow = create_flow(src,dest,srcPort,destPort,ipProto))){
            printf("[Creating flow] (ID:%d Prot:%d) %s:%d->%s:%d\n",flow->id,flow->ipProto,flow->srcIp,flow->srcPort,flow->destIp,flow->destPort);
        }
    }
    fclose(file);
    clearHash(&hash);


    //Testing to compute a key
    /*int flow_id = compute_key(src,dest,srcPort,destPort);
    printf("Key computed: %d\n",flow_id);

    if((flow = create_flow(src,dest,srcPort,destPort))!=NULL){
        printf("Adding flow to allowed flow\n");
        addFlow(flow,&hash);

        unsigned int num_item;
        num_item = HASH_COUNT(hash);
        printf("Number of flows:%u\n",num_item);

        tuple* result = allowed_flow(compute_key(src,dest,srcPort,destPort),hash);
        printf("Retrieving flow from hash table\n");
        if(result != NULL){
            printf("[IP]%s:%d->%s:%d\n",result->srcIp,result->srcPort,result->destIp,result->destPort);

            printf("Deleting flow from hash table\n");
            remove_flow(result,&hash);

            num_item = HASH_COUNT(hash);
            printf("Number of flows:%u\n",num_item);

        }else{
            printf("No result\n");
            free(flow);
            flow=NULL;
        }
  }*/
    return 0;
}
