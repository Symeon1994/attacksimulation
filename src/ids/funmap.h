#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcap.h>
#include "uthash.h"
#define ANY "any"
#define ANYPORT 0

typedef struct {
    char    *destIp;
    u_int   destPort;
    uint8_t funcode;
    uint16_t length;
} fun_record_key;

typedef struct {
  fun_record_key key;
  UT_hash_handle hh;
} fun_record;

fun_record* create_fun_record(char *destIp,u_int destPort,uint8_t funcode,uint16_t length);
void add_fun_record(fun_record** hash, fun_record *record);
fun_record* legal_fun(fun_record** hash,fun_record *record);
void clear_hash_fun_record(fun_record** hash_f);
void line_to_funmap(char line[],char **destIp,u_int* destPort,uint8_t* funcode,uint16_t* length);




