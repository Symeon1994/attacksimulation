#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcap.h>
#include "uthash.h"

#define ANY "any"
#define ANYPORT 0

typedef struct{
     char   *srcIp;
     char   *destIp;
     u_int  srcPort;
     u_int  destPort;
     u_int8_t ipProto;
} flow_key;

typedef struct{
     flow_key key;
     UT_hash_handle hh;
}flow_record;


flow_record* create_flow_record(char *srcIp,char *destIp,u_int srcPort,u_int destPort,u_int8_t ipProto);
void add_flow(flow_record** hash, flow_record *record);
flow_record* legal_flow(flow_record** hash,flow_record *record);
void clear_hash_record(flow_record**  hash_table);
void parse_line(char line[],char **srcIp,char **destIp,u_int *srcPort,u_int *destPort,u_int8_t *ipProto);
void display_flow(flow_record* flow);
