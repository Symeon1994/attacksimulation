#include "flows.h"
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_8201Q (0x8100)

void parse_line(char line[],char **srcIp,char **destIp,u_int *srcPort,u_int *destPort,u_int8_t *ipProto){
    char *word;
    word=strtok(line,"\t");
    //1st word
    *srcIp = word;
    word = strtok(NULL,"\t");
    //2nd word
    //if(strncmp(word,ANY,3)==0){
    //    *srcPort=ANYPORT;
    //} else{
    //    *srcPort = atoi(word);
    //}
    *srcPort = atoi(word);
    word = strtok(NULL,"\t");
    //3rd word
    *destIp = word;
    word = strtok(NULL,"\t");
    //Last word
    //if(strncmp(word,ANY,3)==0){
    //    *destPort=ANYPORT;
    //} else {
    //    *destPort = atoi(word);
    //}
    *destPort = atoi(word);
    word = strtok(NULL,"\t");
    *ipProto = atoi(word);
}

void display_flow(flow_record* flow){
     printf("(Prot:%d) %s:%d<>%s:%d\n",
                        flow->key.ipProto,flow->key.srcIp,flow->key.srcPort,
                        flow->key.destIp,flow->key.destPort);

}
void clear_hash_record(flow_record** hash_table){
     flow_record* current,*tmp;
     HASH_ITER(hh,*hash_table,current,tmp){
         HASH_DEL(*hash_table,current);
         free(current);
     }
}

flow_record* create_flow_record(char *srcIp,char *destIp,u_int srcPort,u_int destPort,u_int8_t ipProto){
    flow_record* flow;
    flow = (flow_record*) malloc (sizeof(flow_record));
    if(flow!=NULL){
        memset(flow,0,sizeof(flow_record));
        (flow->key).srcIp = (char*) malloc(16*sizeof(char));
        (flow->key).destIp = (char *)malloc(16*sizeof(char));
        if((flow->key).srcIp || (flow->key).destIp){
            (flow->key).srcIp = srcIp;
            (flow->key).destIp = destIp;
        } else {
            printf("[create_flow]-Could not allocate memory for flow");
            free(flow);
            flow=NULL;
            return NULL;
        }
        flow->key.srcPort = srcPort;
        flow->key.destPort = destPort;
        flow->key.ipProto = ipProto;
        return flow;
    } else {
        printf("[create_flow]-Could not allocate memory for flow");
        return NULL;
    }
}

void add_flow(flow_record** hash, flow_record *record){
    HASH_ADD(hh,*hash,key,sizeof(flow_key),record);
}

flow_record* legal_flow(flow_record** hash,flow_record *record){
    flow_record* flow;
    HASH_FIND(hh,*hash, &(record->key),sizeof(flow_key),flow);
    return flow;
}



