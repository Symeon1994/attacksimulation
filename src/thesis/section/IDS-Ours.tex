%-----------------------%
% Corrected: 28/05/2016 %
%-----------------------%
\section{Our IDS}
We implemented our own IDS which is a behavior based IDS\@. It is important
to say that IDS has been for implemented with the simulator in mind. Therefore,
it has been mostly designed for the MODBUS protocol as it is the protocol
used in our simulation. The IDS can easily be extended to support other protocol
though. We implemented this IDS to see if the trace we have generated could be
used to evaluate an IDS. It can monitor live traffic but we will use it to read
pcap file.

\subsection{Approach}
This IDS is a behavior based IDS using a whitelist approach. We went for a
whitelist because SCADA networks are more stable than traditional IP networks.
Components of the network are not removed or added as much as in an IP networks.
Because it is a behavior IDS, it has two phases: a learning phase and
the monitoring phase. During the learning phase, the IDS will build two
whitelists. The first whitelist will be a flow whitelist\cite{ramin01}
containing the legal flow in the system. A flow is defined by the source
IP address, destination IP address, source port, destination port and
the IP protocol. All the
packets with the same source/destination IP address, source/destination port and
IP protocol belong to the same flow. The second whitelist details for
each MODBUS server present in the system, the functions code and the
corresponding length of the PDU, we will refer to it as the MODBUS whitelist.
A MODBUS server is identified by its IP address,
the port it uses and the IP protocol used. We decided to make a second whitelist
because we thought that the first one would not be able to detect MODBUS
oriented attack.

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \node[] (start){};
        \node[right =of start,shape=rectangle,draw=black](pcap){pcap};
        \node[right =of pcap](intersection){};
        \node[right =of intersection, shape=rectangle,draw=black](wl1){Flow Whitelist};
        \node[above =of wl1, shape=rectangle, draw=black](learning){Learning Phase};
        \node[right =of wl1, shape=rectangle, draw=black](wl2){MODBUS Whitelist};
        \node[below =of wl1, shape=rectangle, draw=black](detection){Detection Phase};
        \node[below =of detection](alarm){Alarm};

        \draw[->,line width=0.3mm] (start) -- (pcap);
        \draw[-,line width=0.3mm]  (pcap) -- (intersection.center);
        \draw[->,line width=0.3mm] (intersection.center) |- (learning);
        \draw[->,line width=0.3mm] (intersection.center) |- (detection);
        \draw[->,line width=0.3mm] (learning) -- (wl1);
        \draw[->,line width=0.3mm] (wl1) -- (detection);
        \draw[->,line width=0.3mm] (learning) -| (wl2);
        \draw[->,line width=0.3mm] (wl2) |- (detection);
        \draw[->,line width=0.3mm] (detection) -- (alarm);
    \end{tikzpicture}
    \caption{Phase of the IDS}
\end{figure}


Once the learning phase is over, the IDS enters in the monitoring phase.
For each packet, it will first check if it belongs to a whitelisted flow. If
it does, it will determine if it is a MODBUS packet sent to a MODBUS server,
by analyzing the destination address/port and the IP protocol. If it is a MODBUS
packet sent to a MODBUS server, it will perform a lookup in the MODBUS whitelist
to see whether the function code and the length field in the MODBUS packet for
the corresponding server are present.
If not the packet is considered as malicious. If the packet belongs to a
whitelisted flow but it is not malicious, no alert is raised. It is important
to mention that the IDS will perform a lookup in the MODBUS whitelist only if
the packet belongs to a legal flow. Also, the IDS is only able to raise alert,
it cannot drop packet. This is due to the nature of IDS, which is behavior
based and have a high false alarm rate. Unnecessary dropping packet in this
kind of system can have massive impact.


\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
            \node [shape=rectangle,draw=black] (start){sniff packet};
            \node [left = 3 cm of start] (tmp3) {};
            \node [left = of tmp3](tmp4){};
            \node [below =of start,draw=black,shape = rectangle] (flow){Extract flow from packet};
            \node [below =of flow,shape=diamond,draw=black,aspect=2] (whitelist1){flow in whitelist?};
            \node [right =of whitelist1,draw=black,shape = rectangle] (alert1){raise alert};
            \node [below =of whitelist1,draw=black,shape = rectangle] (modbus) {Determine if MODBUS flow};
            \node [below =of modbus,shape=diamond,draw=black,aspect=2] (modbusq) {Is it a Modbus flow?};
            \node [left =of modbusq] (tmp1) {};
            \node [below =of modbusq,draw=black,shape = rectangle] (funcode) {Extract Modbus Server and (funcode,length)};
            \node [below =of funcode,shape=diamond,draw=black,aspect=2,text width=4cm,align = center] (whitelist2) {valid Modbus Server and (funcode,length)?};
            \node [right =of whitelist2,draw=black,shape = rectangle] (alert2) {raise alert};
            \node [left =of whitelist2] (tmp2){};

            \draw [->,line width=0.5mm] (start) -- (flow);
            \draw [->,line width=0.5mm] (flow) -- (whitelist1);
            \draw [->,line width=0.5mm] (whitelist1) -- node[anchor=south] {no} (alert1);
            \draw [->,line width=0.5mm] (alert1) |- (start);
            \draw [->,line width=0.5mm] (whitelist1) -- node[anchor=west] {yes}(modbus);
            \draw [->,line width=0.5mm] (modbus) -- (modbusq);
            \draw [->,line width=0.5mm] (modbusq) -- node[anchor=west] {yes}(funcode);
            \draw [->,line width=0.5mm](modbusq) -- node[anchor=south] {no} (modbusq -| tmp3) |- (start) ;
            \draw [->,line width=0.5mm] (funcode) -- (whitelist2);
            \draw [->,line width=0.5mm] (whitelist2) -- node[anchor=south] {no} (alert2);
            \draw [->,line width=0.5mm] (whitelist2) -- node[anchor=south] {yes} (whitelist2 -| tmp4) |- (start);
            \draw [->,line width=0.5mm] (alert2) |- (start);
    \end{tikzpicture}
    \caption{IDS Activity Diagram}
\end{figure}

In~\ref{behavior-ids}, we mentioned that a behavior based IDS might
consider an attack as legal if it has been performed during the learning
phase. To avoid this problem, the IDS can achieve the learning phase in
offline mode, meaning it generates the whitelist from trace. If the trace is
 known to be safe and representative of what happens in the system, then
the problem is avoided.


\subsection{Improvements}
A limitation that the IDS has, is that the whitelist cannot be edited to add or
remove flow according to the changes in the topology of the network. Even though
SCADA networks tends to be more stable, it does not mean that the topology is
fixed. Furthermore, if the learning phase does not capture the whole behavior
of the system, the IDS will generate false positive. Flow triggering those false
positive should be integrated to the whitelist dynamically by an operator. Based
on his/her knowledge and experience, he/she would decide whether to add the flow or
not.

\subsection{Implementation}
The IDS has been implemented in C. We used the \textit{libpcap} library to sniff
packets. \textit{Libpcap}\cite{libpcap} is widely used and provides a lot of
documentation which make it easy to use. To implement the whitelist, we decided
to use a hash table\cite{uthash}.
Hash table are efficient in terms of lookup which is exactly what we need in
order to make the IDS efficient. If the analysis of a packet is too long,
the IDS may miss some packets. We had to choose which hash function provided
by\cite{uthash} would be the most efficient for the flows. We run tests in
terms of speed and the number of collisions. The different hash function
displayed approximately the same performance so we decided to keep the
default one, which is the \textit{Jenkins} hash function\cite{hashfunction}.
