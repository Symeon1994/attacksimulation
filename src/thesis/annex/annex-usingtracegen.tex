\chapter{Simulation}

This appendix will contain all instructions that are used to manage the simulation.
The first part will tell you how to run the simulation and the second one will
give you all commands that are used with the simulation. Do not forget to go through
the installation appendix before trying to use the simulation.

\section{Installation}

For the simulation you will need to install the Libmodbus library, as it is the
library we use to have ModbusTCP support.

To install libmodbus, you can either go to the website and follow the instructions
to configure it, or you can go into your debian package manager and download libmodbus-3.0.5
(the version we used).

Once you have downloaded libmodbus, in order to be able to compile our code, you will
possibly have to modify the linkage in the make file. Run the following command to
have the path of the libmodbus library on your system.

\begin{lstlisting}
$sudo apt-get install pkg-config
$pkg-config --cflags -libs libmodbus
\end{lstlisting}
Then go in \textit{src/tracegen/Makefile} and change the \textbf{INCLUDE} variable
with the proper path.

\section{Build and Run}

To build the simulation simply use the Makefile. You can clean all files using the
same Makefile.
\begin{lstlisting}
$ make
$ make clean
\end{lstlisting}

Then you will need to launch two instances of the terminal. In the first instance
you will first launch all the processes of the PLC with the \textit{server} program
and then you will launch the instances of the control server with the \textit{master}
program. If you stop the control server, it will automatically stop the PLC processes.
\begin{lstlisting}
$ ./server
$ ./master
\end{lstlisting}

At this point, the simulation has not started yet. You need to launch the HMI for
it to begin. To launch the HMI, simply use on the other terminal instance the \textit{hmi}
program.
\begin{lstlisting}
$ ./hmi
>
\end{lstlisting}

If you look at the terminal instance running the control server, you can see informations
about the running simulation. These are debugging information, that a normal user would
not see. On the HMI terminal, you will get a character '>' telling you that you can
now enter instructions.

\section{Commands}

Now that the simulation is running you will need instructions to manage it, in this
part we will give you a quick explanation of all instructions. There is the list of all instructions.

\begin{description}
\item[help] display the help, concretely, it displays all the available commands
\item[start] start the simulation and set all variables for the power plant to produce
20\% of electricity
\item[stop] stop the power plant
\item[quit] quit the simulation
\item[get] display the content of one register, you will have to specify a register
\begin{itemize}
\item dropped amount
\item current rack
\item furnace temperature
\item furnace coal
\item cuve water
\item cuve steam
\item cuve temperature
\item turbine rotation
\item battery charge
\end{itemize}
\item[status] display the status of the whole power plant
\item[drop] drop the amount of coal specified
\item[rotate] rotate the coal bringer
\item[limit rotation] limit the rotation speed of the turbine (0 to 100)
\item[limit battery] limit the battery content (0 to 100)
\item[flow water] flow water in or out the cuve
\item[constraint] create a constraint on one parameter of the power plant, you
will have to define first the parameter and then the integer value
\begin{itemize}
\item COAL
\item FURNACE\_TEMP
\item WATER\_TEMP
\item WATER
\item STEAM
\item TURBINE
\item BATTERY
\end{itemize}
\end{description}

\section{Modifications}

In this section we are going to explain the different parts of the program that have
to be changed in order to modify the simulation.

\subsection{PLC}

We have a structure that is fixed for the PLC. They are two functions. One that constitute
the operation of the process running the PLC and one function used to update the logic
of the PLC. The function for the process is often the same, it listens and answer to
ModbusTCP query and sometimes initializes state variables. To add PLC, create your
own functions.

Below we can see the prototypes of these functions, both the operations and the
update.
\begin{lstlisting}
// powerplant.h
...
// Operations
void op_coaldoorplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_coalbringerplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_furnaceplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_waterplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_turbineplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_batteryplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);

// Update functions
void rotation_over(int signal);
void updateFurnace(int signal);
void updateWater(int signal);
void updateTurbine(int signal);
void updateBattery(int signal);
...
\end{lstlisting}

Below you can see the structure of the operation functions. Almost all of the functions
looks like this one. It listens to the Modbus query and answer to them. And it defines
an alarm for the update function.
\begin{lstlisting}[language=C]
//powerplant.c
...
void op_batteryplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  _lastUpdate = time(NULL);
  _stateA = 0;
  // Set alarm
  signal(SIGALRM, updateBattery);
  alarm(DEFAULT_UPDATE_TIME);
  while(TRUE) {
    uint8_t request[64];
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);
    modbus_reply(ctx, request, request_len, map);
  }
}
...
\end{lstlisting}

When the functions of the PLC have been defined you can also modify the number of registers
for each type of register and initialize their content with the functions that are below.
\begin{lstlisting}
// powerplant.h
void getOp(int constant, modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
int getCoil(int constant);
int getHolding(int constant);
int getInput(int constant);
int getDiscrete(int constant);
void init_map(modbus_mapping_t* map, int constant);
\end{lstlisting}

The PLC process are launched in the \textit{server.c} file, they are launch using
a \textit{plc()} function which is defined below. You will have to add a constant
for your PLC to be launched, because the constant is used to increment the port on
which the Modbus server will be opened.
\begin{lstlisting}
// Create a new plc
// @param in_pipe input pipe couple
// @param out_pipe output pipe couple
// @param constant the constant that will be added to the PORT to make it unique
// @param reg the parameters for the modbus_mapping_t, the amount of register of the 4 types
// @param op the function for operations;
void plc(int in_pipe[2], int out_pipe[2], int constant);
\end{lstlisting}

Another modification will occur on the control server, you will have to connect the control
server to the PLC. Finally, you have to send the proper pipe to the proper PLC to
be able to give to the PLC the possibility to communicate between each others.

\subsection{Control Server and HMI}

The control server demands not much modifications. You will have to connect the server
to every new PLC you have to rule. The control server will handle the logic of the system,
you can modify the constraint system and the action system to work with brand new
elements that have been defined for your requirements.

The HMI will have to be modified according to the modifications in the control server.
By comparing string you can easily add commands in the HMI, you will find a way easily
in the code.

Finally, there are controller functions that have been defined in two files, one for the HMI to communicate with
the control server (\textit{hmi.c}) and one for the control server to communicate with the
PLC (\textit{controller.c}). You can add and remove functions easily following the different
models that are implemented.
