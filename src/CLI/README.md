MODBUS  Command Line Interface
=============================================

Requirements
------------
The command line interface uses:

    - python-nmap 0.4.4
    - scapy

There are 3 attacks implemented:

    - Port Scanning
    - Function Bruteforce
    - Denial of Service

You can see the attack with the following Command
> show exploits

The terminal will print the attack (there is a simple attack but
    it was for debugging don't pay attention).
To select the attack, run the following command
> select <attack>

Where <attack> corresponds to the name of the attack previously
printed.
Each attack has a set of parameters, you can see those by typing:
> show parameters

The parameters have value by default but you can change them with:
> set <parameter> <value>

When the parameters have been set, you run the attack with
> run

Bruteforce
----------
The parameters are:

    - DESTIP : The IP address of the target
    - DESTPORT : Port on which MODBUS server runs (502 for ex.)
    - INTERIME : The between each request.

Port Scanning
-------------
The parameters are:

    - QUICKSCAN : When true the, it simply tries a 3-way handshake, false will
    send more probe to gain more information
    - RANGE : The range of port to scan. For example to scan port from 22 to 443

> set RANGE 22-443

Denial of Service
-----------------
There are to kind of denial of service, the size of the packet send which too
big and Force Listen Mode Only in MODBUS.

The parameters are:

     - SIZEDOS: true = Denial of service based on the size of the packet,
                 otherwise Force Listen Mode Only.
