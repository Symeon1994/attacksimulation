from _exploit import Exploit
from _constants import *
from random import randint
from scapy.all import *
import nmap

class ScanExploit(Exploit):
    """Simple exploit sending a packet"""

    def __init__(self):
        self.params =  {RANGE: '5020',
                        SRCIP: '127.0.0.1',
                        DESTIP: '127.0.0.1' ,
                        QUICKSCAN: 'true'
                       }

    def find_port(self,line):
        port = ""
        for i in range(len(line)):
            if line[i] == 'g':
                j = i+1
                while not line[j]=="-":
                    if line[j] !=" ":
                        port+= line[j]
                    j+=1
        return port


    def run(self):
        nm = nmap.PortScanner()
        ports = self.params[RANGE]
        if self.params[QUICKSCAN] == "true":
            src_ip = self.params[SRCIP]
            src_port = RandShort()
            args = '-S %s -g %s -p %s -sS -r ' % (src_ip,src_port,ports)
            nm.scan(self.params[DESTIP],arguments=args)
            port_used = self.find_port(nm.command_line())
            f = open("attacker.txt","a+")
            f.write(str(self.params[SRCIP]) + ":" + str(port_used) + "\n")
            f.close()
        else:
            nm.scan(self.params[DESTIP],ports)
        self.display_result(nm)

    def __str__(self):
        return "scan"

    def display_result(self,nm):
        for host in nm.all_hosts():
            print('----------------------------')
            print('Host : %s (%s)' %(host,nm[host].hostname()))
            print('State : %s' % nm[host].state())
            for proto in nm[host].all_protocols():
                print('------------')
                print('Protocol : %s' %proto)
                lport = nm[host][proto].keys()
                lport.sort()
                for port in lport:
                    print('port: %s\tstate : %s' % (port,nm[host][proto][port]['state']))



