from scapy.all import *

class ModbusADU(Packet):
    name="ModbusADU"
    fields_desc=[
        ShortField("transactionID",0),
        ShortField("protocolID",0),
        ShortField("length",6),
        ByteField("unitID",0)
    ]

class ModbusSinglePDU(Packet):
    name="ModbusSinglePDU"
    fields_desc = [
        ByteField("funcode",1),
        ShortField("address",0),
        ShortField("numreg",1)
    ]

class ModbusMultipleCPDU(Packet):
    name="ModbusMultipleRPDU"
    fields_desc = [
        ByteField("funcode",15),
        ShortField("address",0),
        ShortField("numreg",1),
        BitFieldLenField("bytecount",None,8, count_of="outputval",adjust=lambda pkt,x:x),
        FieldListField("outputval",[0],ByteField("noname",0),count_from= lambda pkt: pkt.bytecount)
    ]

class ModbusMultipleRPDU(Packet):
    name="ModbusMultipleCPDU"
    fields_desc = [
        ByteField("funcode",16),
        ShortField("address",0),
        ShortField("numreg",1),
        BitFieldLenField("bytecount",None,8,count_of="outputval",adjust=lambda pkt, x:x),
        FieldListField("outputval",[0],ShortField("noname",0),count_from=lambda pkt: pkt.bytecount)
    ]

class ModbusMaskPDU(Packet):
    name="ModbusMaskPDU"
    fields_desc=[
        ByteField("funcode",22),
        ShortField("refaddress",0),
        ShortField("andmask",0),
        ShortField("ormask",0)
    ]

class ModbusMultipleRWPDU(Packet):
    name="ModbusMultipleRWPDU"
    fields_desc = [
        ByteField("funcode",23),
        ShortField("addressread",0),
        ShortField("qtyread",1),
        ShortField("addresswrite",0),
        ShortField("qtywrite",1),
        BitFieldLenField("bytecount",None,8,count_of="outputval",adjust=lambda pkt, x:x),
        FieldListField("outputval",[0],ShortField("noname",0),count_from=lambda pkt: pkt.bytecount)
    ]

class ModbusReadFIFOPDU(Packet):
    name="ModbusReadFIFOPDU"
    fields_desc = [
        ByteField("funcode",24),
        ShortField("pointer",0)
    ]

class ModbusMEIPDU(Packet):
    name="ModbusMEIPDU"
    fields_desc = [
        ByteField("funcode",43),
        ByteField("type",14),
        ByteField("codeID",1),
        ByteField("objectID",0)

    ]


