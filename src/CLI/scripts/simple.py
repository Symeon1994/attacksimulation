from _exploit import Exploit
from _constants import *

class SimpleExploit(Exploit):
    """Simple exploit sending a packet"""

    def __init__(self):
        self.params =  {SRCIP: None,
                        SRCPORT: None,
                        DESTIP: None,
                        DESTPORT: None,
                        PROTO: None}



    def run(self):
        print "Running the exploit"

    def __str__(self):
        return "simple"
