# !/usr/bin/env python
from scapy.all import *
import sys
# Set log level to benefit from Scapy warnings

INIT_FILE = 1
INJECT_FILE = 2
OUTPUT_FILE = 3

'''
Begin injection of packet
'''
packets = rdpcap(sys.argv[INIT_FILE])
injection = rdpcap(sys.argv[INJECT_FILE])

result = []

if len(packets) <= len(injection):
    i = 0
    j = 0
    for i in range(len(packets)):
        if i % 2 == 0:
            result.append(packets[i])
        else:
            if j < len(injection):
                result.append(injection[j])
                j +=1
                i -=1
            else:
                result.append(packets[i])

wrpcap(sys.argv[OUTPUT_FILE],result)
