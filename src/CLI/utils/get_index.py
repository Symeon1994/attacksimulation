# !/usr/bin/python

import sys

INPUT_FILE = 1
OUTPUT_FILE = 2

inp = open(sys.argv[1],'r')
out = open(sys.argv[2],'w')

for line in inp:
    index = line.split('\t',1)
    out.write(index[0] + '\n')

inp.close()
out.close()
