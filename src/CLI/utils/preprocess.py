# !/usr/bin/python

from scapy.all import *
import sys

INPUT_FILE = 1
OUTPUT_FILE = 2

#TCP = 6
ICMP = 1
SYN = 'S'
ECHO_REQUEST = 8

in_file = rdpcap(sys.argv[INPUT_FILE])
"""
def preprocess(in_trace):
    result = []
    for packet in in_trace:
        if (packet.sprintf('%TCP.flags%') ==SYN and packet.proto == TCP) or\
                (packet.proto ==ICMP and packet[ICMP].type == ECHO_REQUEST:
            result.append(packet)
    return result
"""
def tamper(in_trace):
    result = []
    for packet in in_trace:
        if packet[IP].src == '127.0.0.1' and (packet[TCP].sport >= 2590 and packet[TCP].sport <8080) or\
                packet[IP].dst == '127.0.0.1' and (packet[TCP].dport >= 2590 and packet[TCP].dport < 8080):
            if packet[TCP].sport == 2590:
                packet[IP].src = '192.73.0.0'
                packet[TCP].sport =502

            elif packet[TCP].dport == 2590:
                packet[IP].dst = '192.73.0.0'
                packet[TCP].dport = 502

            elif packet[TCP].sport == 2591:
                packet[IP].src = '192.73.0.1'
                packet[TCP].sport =502

            elif packet[TCP].dport == 2591:
                packet[IP].dst = '192.73.0.1'
                packet[TCP].dport = 502

            elif packet[TCP].sport == 2592:
                packet[IP].src = '192.73.0.2'
                packet[TCP].sport = 502

            elif packet[TCP].dport ==2592:
                packet[IP].dst = '192.73.0.2'
                packet[TCP].dport = 502

            elif packet[TCP].sport == 2593:
                packet[IP].src = '192.73.0.3'
                packet[TCP].sport = 502


            elif packet[TCP].dport == 2593:
                packet[IP].dst ='192.73.0.3'
                packet[TCP].dport=502

            elif packet[TCP].sport == 2594:
                packet[IP].src = '192.73.0.4'
                packet[TCP].sport = 502

            elif packet[TCP].dport == 2594:
                packet[IP].dst = '192.73.0.4'
                packet[TCP].dport = 502

            elif packet[TCP].sport == 2595:
                packet[IP].src = '192.73.0.5'
                packet[TCP].sport = 502

            elif packet[TCP].dport == 2595:
                packet[IP].dst = '192.73.0.5'
                packet[TCP].dport = 502

        result.append(packet)
    return result

result = tamper(in_file)
wrpcap(sys.argv[OUTPUT_FILE],result)

