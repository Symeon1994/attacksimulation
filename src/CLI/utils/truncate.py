# !/usr/bin/python

from scapy.all import *
import sys

INPUT_FILE = 1
OUTPUT_FILE = 2
NBR_PACKET = 3

in_file = rdpcap(sys.argv[INPUT_FILE])
nbr = sys.argv[NBR_PACKET]
wrpcap(sys.argv[OUTPUT_FILE],in_file[0:int(nbr)])
