# !/usr/bin/python

from scapy.all import *
import sys

INPUT_FILE = 1
INDEX_FILE = 2
ATTACK_LIST = 3

true_positive = 0
false_positive = 0
true_negative = 0
false_negative = 0

# in index_list and in attack_list
# in index_list and not in attack_list
# not in index_list and in attack_list
# not in index_list and not in attakc_list

attacker_list = set()
index_list = []

file_index = open(sys.argv[INDEX_FILE],'r')
file_attacker = open(sys.argv[ATTACK_LIST],'r')

#Creating the list of index
for line in file_index:
    index_list.append(str(line).replace("\n",""))

#Creating the set of attacker
for line in file_attacker:
    attacker_list.add(str(line).replace("\n",""))

trace = rdpcap(sys.argv[INPUT_FILE])
current_index = 0

for packet in trace:
    current_index += 1
    source = str(packet[IP].src) + ":" + str(packet[TCP].sport)
    dest = str(packet[IP].dst) + ":" + str(packet[TCP].dport)
    if (str(current_index) in index_list) and ((source in attacker_list) or (dest in attacker_list)):
        # A malicious attack that trigger an alarm
        true_positive += 1
    elif (str(current_index) in index_list) and ((source not in attacker_list) or (dest not in attacker_list)):
        # A legitimate packet that trigger and alarm
        false_positive += 1
    elif (str(current_index) not in index_list) and ((source in attacker_list) or (dest in attacker_list)):
        # A malicious packet that did not trigger and alarm
        false_negative += 1
    elif (str(current_index) not in index_list) and ((source not in attacker_list) or (dest not in attacker_list)):
        # A legitimate packet that did not trigger and alarm
        true_negative += 1

print "True Negative: " + str(true_negative) + " False Negative: " + str(false_negative) + "\n"
print "True Positive: " + str(true_positive) + " False Positive: " + str(false_positive) + "\n"
