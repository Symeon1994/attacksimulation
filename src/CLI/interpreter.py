from tabulate import tabulate
from scripts.simple import SimpleExploit
from scripts.scan import ScanExploit
from scripts.bruteforce import BruteforceExploit
from scripts._constants import *
from scripts.dos import DosExploit
from os import listdir
from os.path import isfile, join
import cmd

class Interpreter(cmd.Cmd):
    """Command processor to choose the exploit"""

    def __init__(self,intro="Modbus attack tool",prompt=">> "):
        cmd.Cmd.__init__(self)
        self.intro = intro
        self.prompt = prompt
        path = "./scripts"
        self.exploits = [f[:-3] for f in listdir(path) if not f.startswith("_") and\
                                        f.endswith("py") and\
                                        isfile(join(path, f))]

    def do_show(self, line):
        if line == "exploits":
            #TODO find a way to display Description headers = ["Title", "Description"]
            headers = ["Title"]
            table = [[f] for f in self.exploits]
            print tabulate(table, headers, tablefmt="rst")

    def help_show(self):
        print "show <>\n"
        print "\t detailed information"

    def do_exit(self, line):
        return True

    def help_exit(self):
        print "exit:\n"
        print "\t quit the command line tool"

    do_EOF = do_exit
    help_EOF = help_exit

    def do_select(self,line):
        if line in self.exploits:
            construct = globals()[(line.capitalize())+SUFFIX]
            args = construct()
            ex_cli = ExploitInterpreter(args)
            ex_cli.cmdloop()
        else:
            print "The exploit [%s ] does no exists" %line

    def complete_select(self, text, line, begidx, endidx):
        if not text:
            completions = self.exploits[:]
        else:
            completions = [f
                           for f in self.exploits
                           if f.startswith(text)
                          ]
    def help_select(self):
        print "select <exploit>:\n"
        print "\t select the exploit that will be used"

    def emptyline(self):
        pass

class ExploitInterpreter(Interpreter):
    def __init__(self,args):
        Interpreter.__init__(self,intro="",prompt="("+str(args)+")>> ")
        self.args = args

    def do_show(self,line):
        if line == "parameters":
            self.args.show_params()

    def help_show(self):
        print "show <>\n"
        print "\t detailed information"

    def do_set(self, line):
        res = line.split(' ')
        if len(res)>2 and len(res)%2 == 0:
            for i in range(0,len(res),2):
                self.args.set(res[i],res[i+1])
        self.args.set(res[0], res[1])

    def do_run(self, line):
        self.args.run()
        return True

    def help_run(self):
        print "run:\n"
        print "\t run the chosen exploit"

if __name__ == '__main__':
    Interpreter().cmdloop()
