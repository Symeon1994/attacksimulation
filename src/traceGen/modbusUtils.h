#ifndef MODBUS_UTILS_H
#define MODBUS_UTILS_H

#include <modbus.h>

/**
 * Open a modbus TCP server
 * @param ip_address the IPv4 address
 * @param port the port
 * @return the modbus connection, or NULL if it failed
 */
modbus_t* openServer(const char* ip_address, int port);

/**
 * Close a modbus server
 * @param mb a pointer to the modbus server
 */
void closeServer(modbus_t* mb);

/**
 * Open a modbus connection
 * @param ip_address the IPv4 address
 * @param port the port
 * @return the modbus connection, or NULLl it failed
 */
modbus_t* openConnection(const char* ip_address, int port);

/**
 * Close a modbus server connection
 * @param mb a pointer to the modbus server
 */
void closeConnection(modbus_t* mb);

#endif
