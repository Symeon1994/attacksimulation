#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <modbus.h>
#include <errno.h>
#include <time.h>
#include "plc.h"
#include "powerplantplc.h"
#include "modbusUtils.h"
#include "constant.h"

void plc(int in_pipe[2], int out_pipe[2], int constant, char* ip_address,int port) {
  pid_t pid = fork();
  if(pid == 0) {
    // Closing input writing
    if(in_pipe != NULL)
      close(in_pipe[1]);
    // Closing output reading
    if(out_pipe != NULL)
      close(out_pipe[0]);

    // Mapping
    modbus_mapping_t* map = modbus_mapping_new(
      getCoil(constant),
      getDiscrete(constant),
      getHolding(constant),
      getInput(constant)
    );
    if(map == NULL) {
      fprintf(stderr, "Error mapping!\n");
      if(in_pipe != NULL)
        close(in_pipe[0]);
      if(out_pipe != NULL)
        close(out_pipe[1]);
      exit(-1);
    }
    init_map(map, constant);

    // Connection modbus
    //modbus_t* ctx = openServer(IP_ADDRESS, PORT+constant);
    modbus_t* ctx = openServer(ip_address, port+constant);
    modbus_set_debug(ctx, FALSE);
    int socket = modbus_tcp_listen(ctx, 1);
    if(socket == -1) {
      fprintf(stderr, "Error creating socket: %s\n", modbus_strerror(errno));
      exit(-1);
    }
    modbus_tcp_accept(ctx, &socket);

    // Main loop
    getOp(constant, ctx, map, in_pipe == NULL ? -1 : in_pipe[0], out_pipe == NULL ? -1 : out_pipe[1]);

    // Freeing
    modbus_mapping_free(map);
    closeServer(ctx);
    if(in_pipe != NULL)
      close(in_pipe[0]);
    if(out_pipe != NULL)
      close(out_pipe[1]);
  }
}
