#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <modbus.h>
#include "modbusUtils.h"

modbus_t* openServer(const char* ip_address, int port) {
  printf("Trying to create server %s:%d\n", ip_address, port);
  modbus_t *mb = modbus_new_tcp(ip_address, port);

  // Creating modbus connection
  if(mb == NULL) {
    fprintf(stderr, "Error trying to open a modbus connection!\n");
    return NULL;
  }
  return mb;
}

void closeServer(modbus_t* mb) {
  if(mb == NULL)
    return;
  modbus_close(mb);
  modbus_free(mb);
}

modbus_t* openConnection(const char* ip_address, int port) {
  modbus_t *mb = modbus_new_tcp(ip_address, port);
  // Creating modbus connection
  if(mb == NULL) {
    fprintf(stderr, "Error: %s\n", modbus_strerror(errno));
    return NULL;
  }
  // Connection establishment
  if(modbus_connect(mb) == -1) {
    fprintf(stderr, "Error: %s\n", modbus_strerror(errno));
    modbus_free(mb);
    return NULL;
  }
  return mb;
}

void closeConnection(modbus_t* mb) {
  if(mb == NULL)
    return;
  modbus_close(mb);
  modbus_free(mb);
}
