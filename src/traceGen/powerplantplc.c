#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <modbus.h>
#include "powerplantplc.h"
#include "constant.h"
#include "physics.h"

modbus_mapping_t* _map = NULL;
int _rfd = 0;
int _wfd = 0;
time_t _lastUpdate;

unsigned long _stateA;
unsigned long _stateB;

void getOp(int constant, modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
    switch(constant) {
      case PLC_COAL_DOOR: op_coaldoorplc(ctx, map, rfd, wfd);
      case PLC_COAL_BRINGER: op_coalbringerplc(ctx, map, rfd, wfd);
      case PLC_FURNACE: op_furnaceplc(ctx, map, rfd, wfd);
      case PLC_WATER: op_waterplc(ctx, map, rfd, wfd);
      case PLC_TURBINE: op_turbineplc(ctx, map, rfd, wfd);
      case PLC_BATTERY: op_batteryplc(ctx, map, rfd, wfd);
      default: return;
    }
}

int getCoil(int constant) {
  switch(constant) {
    case PLC_COAL_DOOR: return 0;
    case PLC_COAL_BRINGER: return 1;
    case PLC_FURNACE: return 1;
    case PLC_WATER: return 0;
    case PLC_TURBINE: return 1;
    case PLC_BATTERY: return 1;
    default: return 0;
  }
}

int getHolding(int constant) {
  switch(constant) {
    case PLC_COAL_DOOR: return 1;
    case PLC_COAL_BRINGER: return 0;
    case PLC_FURNACE: return 0;
    case PLC_WATER: return 2;
    case PLC_TURBINE: return 1;
    case PLC_BATTERY: return 1;
    default: return 0;
  }
}

int getInput(int constant) {
  switch(constant) {
    case PLC_COAL_DOOR: return 2;
    case PLC_COAL_BRINGER: return TOTAL_RACK_NUMBER + 1;
    case PLC_FURNACE: return 2;
    case PLC_WATER: return 3;
    case PLC_TURBINE: return 1;
    case PLC_BATTERY: return 1;
    default: return 0;
  }
}

int getDiscrete(int constant) {
  switch(constant) {
    case PLC_COAL_DOOR: return 0;
    case PLC_COAL_BRINGER: return 0;
    case PLC_FURNACE: return 0;
    case PLC_WATER: return 0;
    case PLC_TURBINE: return 0;
    case PLC_BATTERY: return 0;
    default: return 0;
  }
}

void init_map(modbus_mapping_t* map, int constant) {
  int i = 0;
  switch(constant) {
    case PLC_COAL_DOOR:
      // Input register holding amount dropped
      for(i = 0 ; i < 2 ; i++)
        map->tab_input_registers[i] = 0;
      break;
    case PLC_COAL_BRINGER:
      // The 8 coal bringer
      for(i = 0 ; i < 8 ; i++)
        map->tab_input_registers[i] = 0;
      break;
    case PLC_FURNACE:
      // Temperature of the furnace
      map->tab_input_registers[0] = START_FURNACE_TEMP;
      // Amount of coal
      map->tab_input_registers[1] = 0;
      // Furnace light up
      map->tab_bits[0] = OFF;
      // Door open/closed
      map->tab_bits[1] = OFF;
      break;
    case PLC_WATER:
      // Water temperature
      map->tab_input_registers[0]= START_WATER_TEMP;
      // Water level
      map->tab_input_registers[1]=0;
      // Steam amount
      map->tab_input_registers[2]=0;
      // Water flowing in 0-100
      map->tab_registers[0]=0;
      break;
    case PLC_TURBINE:
      // Turbine rotation RPM
      map->tab_input_registers[0] = 0;
      // Turbine on/off
      map->tab_bits[0] = OFF;
      // Rotation limitation 0-100
      map->tab_registers[0] = 100;
      break;
    case PLC_BATTERY:
      // Battery charge 0-100
      map->tab_input_registers[0] = 0;
      // Battery connect/disconnect
      map->tab_bits[0] = OFF;
      // Battery limitation 0-100
      map->tab_registers[0] = 100;
      break;
    default: return;
  }
}

void op_coaldoorplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  uint16_t a[1];
  while(TRUE) {
    uint8_t request[64];
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);
    modbus_reply(ctx, request, request_len, map);
    a[0] = map->tab_registers[0];
    map->tab_registers[0] = 0;
    if(a[0] > 0) {
      write(wfd, (char*)a, 2);
      int val = map->tab_input_registers[COAL_DOOR_INPUT_AMOUNT_ADDR] + a[0];
      if(val > 65536) {
        map->tab_input_registers[COAL_DOOR_INPUT_AMOUNT_ADDR+1] += 1;
        map->tab_input_registers[COAL_DOOR_INPUT_AMOUNT_ADDR] = (uint16_t)(val-65536);
      }
      else
        map->tab_input_registers[COAL_DOOR_INPUT_AMOUNT_ADDR] += a[0];
    }
  }
}

void op_coalbringerplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  char data[2];
  int launchedRot = FALSE;
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  while(TRUE) {
    uint8_t request[64];

    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);

    // See amount dropped
    ssize_t r;
    while((r = read(rfd, data, 2)) > 0) {
      uint16_t current = map->tab_input_registers[COAL_BRINGER_INPUT_RACK_ADDR];
      map->tab_input_registers[current] += ((uint16_t*)data)[0];
      if(map->tab_input_registers[current] > MAX_COAL_PER_BRINGER_RACK)
        map->tab_input_registers[current] = MAX_COAL_PER_BRINGER_RACK;
    }
    modbus_reply(ctx, request, request_len, map);
    // If rotation activated
    if(map->tab_bits[COAL_BRINGER_COIL_ROTATE_ADDR] == ON && launchedRot == FALSE) {
      launchedRot = TRUE;
      signal(SIGALRM, rotation_over);
      alarm(BRINGER_ROTATION_TIME);
    }
    else if(map->tab_bits[COAL_BRINGER_COIL_ROTATE_ADDR] == OFF)
      launchedRot = FALSE;
  }
}

void op_furnaceplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  _stateA = 0; // Power in the furnace
  _stateB = 0; // Coal in the furnace
  _lastUpdate = time(NULL);
  // Set alarm
  signal(SIGALRM, updateFurnace);
  alarm(DEFAULT_UPDATE_TIME);
  while(TRUE) {
    uint8_t request[64];
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1) {
      exit(-1);
    }
    modbus_reply(ctx, request, request_len, map);
  }
}

void op_waterplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  _lastUpdate = time(NULL);
  _stateA = 0;
  // Set alarm
  signal(SIGALRM, updateWater);
  alarm(DEFAULT_UPDATE_TIME);
  while(TRUE) {
    uint8_t request[64];
    // Received request
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);
    // Reply to request
    modbus_reply(ctx, request, request_len, map);
  }
}

void op_turbineplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  _lastUpdate = time(NULL);
  // Set alarm
  signal(SIGALRM, updateTurbine);
  alarm(DEFAULT_UPDATE_TIME);
  while(TRUE) {
    uint8_t request[64];
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);
    modbus_reply(ctx, request, request_len, map);
  }
}

void op_batteryplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd) {
  // Alarm variables
  _map = map;
  _rfd = rfd;
  _wfd = wfd;
  _lastUpdate = time(NULL);
  _stateA = 0;
  // Set alarm
  signal(SIGALRM, updateBattery);
  alarm(DEFAULT_UPDATE_TIME);
  while(TRUE) {
    uint8_t request[64];
    int request_len = modbus_receive(ctx, request);
    if(request_len == -1)
      exit(-1);
    modbus_reply(ctx, request, request_len, map);
  }
}


/* ALARM Function */
void rotation_over(int signal) {
  // Rotation done
  _map->tab_bits[COAL_BRINGER_COIL_ROTATE_ADDR] = OFF;

  // Get id of the rack to be dropped in furnace
  uint16_t currentRack = _map->tab_input_registers[COAL_BRINGER_INPUT_RACK_ADDR];
  int i = currentRack + TOTAL_RACK_NUMBER/2;
  if(i >= TOTAL_RACK_NUMBER)
    i -= TOTAL_RACK_NUMBER;

  // Empty it
  uint16_t current = _map->tab_input_registers[i];
  _map->tab_input_registers[i] = 0;

  // Rotate
  currentRack ++;
  if(currentRack >= TOTAL_RACK_NUMBER)
    currentRack -= TOTAL_RACK_NUMBER;
  _map->tab_input_registers[COAL_BRINGER_INPUT_RACK_ADDR] = currentRack;

  // Write amount in furnace
  write(_wfd, (char*)(&current), 2);
}

/*
_map
_rfd
_wfd
_stateA ==> Power in the furnace
_stateB ==> Coal in the furnace in g
*/
void updateFurnace(int sig) {
  char data[2];
  time_t current = time(NULL);
  double sec = difftime(current, _lastUpdate);

  // Read pipe and write coal in register
  ssize_t r;
  while((r = read(_rfd, data, 2)) > 0) {
    _stateB += ((uint16_t*)data)[0]*1000;
  }
  _map->tab_input_registers[FURNACE_INPUT_COAL_ADDR] = _stateB/1000;


  // If furnace is on
  if(_map->tab_bits[FURNACE_COIL_LIGHT_ADDR] == ON || _map->tab_input_registers[FURNACE_INPUT_TEMP_ADDR] > START_FURNACE_TEMP) {
    // Update power
    unsigned long generatedPower = getGeneratedPower(&_stateB, (int)(sec*1000));
    unsigned long powerLoss = getFurnacePowerLoss(_stateA, (int)(sec*1000));
    _stateA -= powerLoss;
    _stateA += generatedPower;
    _map->tab_input_registers[FURNACE_INPUT_TEMP_ADDR] = getAmbientTemperature(_stateA);
  }


  // Write power as output
  write(_wfd, (char*)(&_stateA), sizeof(unsigned long)/sizeof(char));

  // Update time
  _lastUpdate = time(NULL);

  // Prepare next alarm
  signal(SIGALRM, updateFurnace);
  alarm(DEFAULT_UPDATE_TIME);
}


// _stateA ==> WATER POWER
void updateWater(int sig) {
  size_t datalen = sizeof(unsigned long)/sizeof(char);
  char data[datalen];
  time_t current = time(NULL);
  double sec = difftime(current, _lastUpdate);

  // Read furnace power
  ssize_t r;
  int number_input = 0;
  unsigned long furnacePower = _stateA;
  while((r = read(_rfd, data, datalen)) > 0) {
    furnacePower = ((unsigned long*)data)[0];
    number_input++;
  }

  // Nothing to read we do not update, to avoid loosing energy and not gaining any
  // Execution must be in sequence after the furnace
  if(number_input == 0) {
    signal(SIGALRM, updateWater);
    alarm(DEFAULT_UPDATE_TIME);
    return;
  }

  // Reduce steam, it is important to start by removing steam
  int steamLevel = (int)_map->tab_input_registers[WATER_INPUT_STEAM_ADDR];
  if(steamLevel > 0) {
    double steam = getSteamOuput(steamLevel, (int)(sec*1000));
    if(steam > steamLevel)
      steam = steamLevel;
    _map->tab_input_registers[WATER_INPUT_STEAM_ADDR] -= (int)steam;
    write(_wfd, (char*)(&steam), sizeof(double)/sizeof(char));
    //_stateA -= getEnergyForSteam(steam); // Maybe put back?
  }

  // Update power
  unsigned long powerLoss = getWaterPowerLoss(_stateA, (int)(sec*1000));
  unsigned long powerRaise = getPowerTransfer(furnacePower, (int)(sec*1000));
  _stateA -= powerLoss;
  _stateA += powerRaise;
  if(_stateA > furnacePower)
    _stateA = furnacePower;

  // Update temperature
  int waterLevel = (int)_map->tab_input_registers[WATER_INPUT_LVL_ADDR];
  steamLevel = (int)_map->tab_input_registers[WATER_INPUT_STEAM_ADDR];
  _map->tab_input_registers[WATER_INPUT_TEMP_ADDR] = getWaterTemperatureFromPower(_stateA, waterLevel+steamLevel);
  if(_map->tab_input_registers[WATER_INPUT_TEMP_ADDR]  < START_WATER_TEMP)
    _map->tab_input_registers[WATER_INPUT_TEMP_ADDR]  = START_WATER_TEMP;

  // Calculate steam
  int steamProduced = getSteamAmountProduced(_stateA, waterLevel, steamLevel, (int)(sec*1000));
  if(steamProduced > _map->tab_input_registers[WATER_INPUT_LVL_ADDR])
    steamProduced = _map->tab_input_registers[WATER_INPUT_LVL_ADDR];
  _map->tab_input_registers[WATER_INPUT_LVL_ADDR] -= steamProduced;
  _map->tab_input_registers[WATER_INPUT_STEAM_ADDR] += steamProduced;

  // Calculate water level
  uint16_t flow = _map->tab_registers[WATER_HOLDING_FLOWIN_ADDR] - _map->tab_registers[WATER_HOLDING_FLOWOUT_ADDR];
  if(flow > 0) {
    int tmp = (int)((flow * WATER_MAX_FLOW*sec)/100);
    if(tmp < 0 && -tmp > _map->tab_input_registers[WATER_INPUT_LVL_ADDR])
      _map->tab_input_registers[WATER_INPUT_LVL_ADDR] = 0;
    else
      _map->tab_input_registers[WATER_INPUT_LVL_ADDR] += tmp;
  }
  if(_map->tab_input_registers[WATER_INPUT_LVL_ADDR] > WATER_CUVE_CAPACITY)
    _map->tab_input_registers[WATER_INPUT_LVL_ADDR] = WATER_CUVE_CAPACITY;

  // Update time
  _lastUpdate = time(NULL);

  // Prepare next alarm
  signal(SIGALRM, updateWater);
  alarm(DEFAULT_UPDATE_TIME);
}

void updateTurbine(int sig) {
  char data[2];
  time_t current = time(NULL);
  double sec = difftime(current, _lastUpdate);

  // Read steam amount
  ssize_t r;
  double steaminput = 0;
  int line = 0;
  while((r = read(_rfd, data, sizeof(double)/sizeof(char))) > 0) {
    steaminput = ((double*)data)[0];
    line++;
  }

  // If nothing to read
  if(line == 0) {
    signal(SIGALRM, updateTurbine);
    alarm(DEFAULT_UPDATE_TIME);
    return;
  }

  // Calculate turbine rotation
  int rpm = _map->tab_input_registers[TURBINE_INPUT_ROTATION_ADDR];
  int var = getRPMIncreaseFromQte(steaminput, (int)(sec*1000), rpm);
  uint16_t turbineRPM = rpm+var;
  _map->tab_input_registers[TURBINE_INPUT_ROTATION_ADDR] = turbineRPM > 100 ? 100 : turbineRPM;

  // Write output energy
  int outelectricity = getEnergyProduction(rpm > 100 ? 100 : rpm, (int)(sec*1000));
  write(_wfd, (char*)(&outelectricity), sizeof(int)/sizeof(char));

  // Update time
  _lastUpdate = time(NULL);
  // Prepare next alarm
  signal(SIGALRM, updateTurbine);
  alarm(DEFAULT_UPDATE_TIME);

}

void updateBattery(int sig) {
  char data[2];
  time_t current = time(NULL);
  double sec = difftime(current, _lastUpdate);
  // Read energy input
  int inputElec = 0;
  ssize_t r;
  while((r = read(_rfd, data, sizeof(int)/sizeof(char))) > 0) {
    inputElec += ((int*)data)[0];
  }

  // output
  if(_map->tab_registers[BATTERY_COIL_CONNECT_ADDR] == ON)
    _stateA -= (int)(BATTERY_OUTPUT_PER_SECOND*sec);
  if(_stateA < 0)
    _stateA = 0;

  // Input
  _stateA += inputElec;
  int batteryCharge = _stateA/WATT_PER_PERCENT;

  // Update register
  if(batteryCharge > _map->tab_registers[BATTERY_HOLDING_LIMIT_ADDR])
    batteryCharge = _map->tab_registers[BATTERY_HOLDING_LIMIT_ADDR];
  _map->tab_input_registers[BATTERY_INPUT_CHARGE_ADDR] = batteryCharge;

  // Update time
  _lastUpdate = time(NULL);

  // Prepare next alarm
  signal(SIGALRM, updateBattery);
  alarm(DEFAULT_UPDATE_TIME);
}
