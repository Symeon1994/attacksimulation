#include <stdlib.h>
#include <stdio.h>
#include <modbus.h>
#include <unistd.h>
#include <time.h>
#include "constant.h"
#include "modbusUtils.h"
#include "controller.h"

int getTotalAmountDropped(modbus_t* plc[]) {
    uint16_t total[2];
    if(modbus_read_input_registers(plc[PLC_COAL_DOOR], COAL_DOOR_INPUT_AMOUNT_ADDR, 2, total) == -1) {
      fprintf(stderr, "Error trying to get the total amount dropped\n");
      return 0;
    }
    return (int)total[0] + ((int)total[1])*65536;
}

void dropCoal(modbus_t* plc[], uint16_t amount) {
  uint8_t rotate[] = {OFF};
  if(modbus_read_bits(plc[PLC_COAL_BRINGER], COAL_BRINGER_COIL_ROTATE_ADDR, 1, rotate) == -1)
    fprintf(stderr, "Error trying to read rotation status\n");
  if(rotate[0] == ON) {
    sleep(1);
    dropCoal(plc, amount);
  }
  else {
    printf("Dropping coal: %d (%d)\n", amount, getTotalAmountDropped(plc));
    if(modbus_write_register(plc[PLC_COAL_DOOR], COAL_DOOR_HOLDING_DROP_ADDR, amount) == -1)
      fprintf(stderr, "Error trying asking to drop coal\n");
  }
}

uint16_t getCoalAmount(modbus_t* plc[], int i) {
  if(i < 0 || i >= TOTAL_RACK_NUMBER) {
    fprintf(stderr, "The bringer rack that is asked does not exists\n");
    return 0;
  }
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_COAL_BRINGER], COAL_BRINGER_INPUT_COAL_ADDR + i, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get coal amount\n");
    return 0;
  }
  return buffer[0];
}

uint16_t getCurrentRack(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_COAL_BRINGER], COAL_BRINGER_INPUT_RACK_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get current rack\n");
    return 0;
  }
  return buffer[0];
}

void rotateBringer(modbus_t* plc[]) {
  uint8_t rotate[] = {OFF};
  if(modbus_read_bits(plc[PLC_COAL_BRINGER], COAL_BRINGER_COIL_ROTATE_ADDR, 1, rotate) == -1)
    fprintf(stderr, "Error trying to read rotation status\n");
  if(rotate[0] == ON) {
    sleep(1);
    rotateBringer(plc);
  }
  printf("Rotation of the bringer\n");
  if(modbus_write_bit(plc[PLC_COAL_BRINGER], COAL_BRINGER_COIL_ROTATE_ADDR, ON) == -1)
    fprintf(stderr, "Error asking for a rotation\n");
}

void lightFurnace(modbus_t* plc[]) {
  printf("Lighting Furnace\n");
  if(modbus_write_bit(plc[PLC_FURNACE], FURNACE_COIL_LIGHT_ADDR, ON) == -1)
    printf("Error lighting furnace\n");
}

uint16_t getFurnaceTemperature(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_FURNACE], FURNACE_INPUT_TEMP_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get furnace temperature\n");
    return 0;
  }
  return buffer[0];
}

uint16_t getFurnaceCoalAmount(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_FURNACE], FURNACE_INPUT_COAL_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get furnace coal amount\n");
    return 0;
  }
  return buffer[0];
}

uint16_t getWaterLevel(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_WATER], WATER_INPUT_LVL_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get water level");
    return 0;
  }
  return buffer[0];
}

uint16_t getSteamLevel(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_WATER], WATER_INPUT_STEAM_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get steam level\n");
    return 0;
  }
  return buffer[0];
}

uint16_t getWaterTemperature(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_WATER], WATER_INPUT_TEMP_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get water temperature\n");
    return 0;
  }
  return buffer[0];
}

void flowWaterIn(modbus_t* plc[], uint16_t amount) {
  if(amount > 100)
    amount = 100;
  printf("Water flow in [");
  int i;
  for(i = 0 ; i < amount/5 ; i++)
    printf("|");
  for(i = amount/5 ; i < 20 ; i++)
    printf("-");
  printf("] %d%%\n", amount);
  // Change flow
  if(modbus_write_register(plc[PLC_WATER], WATER_HOLDING_FLOWIN_ADDR, amount) == -1)
    fprintf(stderr, "Error trying to flow water in the cuve\n");
  if(modbus_write_register(plc[PLC_WATER], WATER_HOLDING_FLOWOUT_ADDR, 0) == -1)
    fprintf(stderr, "Error trying to stop water flowing out of the cuve\n");
}

void flowWaterOut(modbus_t* plc[], uint16_t amount) {
  if(amount > 100)
    amount = 100;
  printf("Water flow out [");
  int i;
  for(i = 0 ; i < amount/5 ; i++)
    printf("|");
  for(i = amount/5 ; i < 20 ; i++)
    printf("-");
  printf("] %d%%\n", amount);
  // Change flow
  if(modbus_write_register(plc[PLC_WATER], WATER_HOLDING_FLOWIN_ADDR, 0) == -1)
    fprintf(stderr, "Error trying to stop water flowing in the cuve\n");
  if(modbus_write_register(plc[PLC_WATER], WATER_HOLDING_FLOWOUT_ADDR, amount) == -1)
    fprintf(stderr, "Error trying to flow water out the cuve\n");
}

void limitTurbineRotation(modbus_t* plc[], uint16_t limit) {
  printf("\tAdjusting turbine max rotation limit");
  if(modbus_write_register(plc[PLC_TURBINE], TURBINE_HOLDING_ROTLIMIT_ADDR, limit) == -1)
    printf("\t%sERROR%s\n", KRED, KWHITE);
  else
    printf("\t%sDONE%s\n", KGREEN, KWHITE);
}

void enableTurbine(modbus_t* plc[], uint8_t enable) {
  printf("\tTurbine state");
  if(modbus_write_bit(plc[PLC_TURBINE], TURBINE_COIL_SWITCH_ADDR, enable) == -1)
    printf("\t\t\t\t%sERROR%s\n", KRED, KWHITE);
  else
    printf("\t\t\t\t%s%s%s\n",enable == ON ? KGREEN : KRED, enable == ON ? "ON" : "OFF", KWHITE);
}

uint16_t getTurbineRotation(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_TURBINE], TURBINE_INPUT_ROTATION_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get turbine rotation\n");
    return 0;
  }
  return buffer[0];
}

void limitBatteryContent(modbus_t* plc[], uint16_t limit) {
  printf("\tAdjusting Battery max capacity");
  if(modbus_write_register(plc[PLC_BATTERY], BATTERY_HOLDING_LIMIT_ADDR, limit) == -1)
    printf("\t\t%sERROR%s\n", KRED, KWHITE);
  else
    printf("\t\t%sDONE%s\n", KGREEN, KWHITE);
}

void connectBattery(modbus_t* plc[], uint8_t enable) {
  // Connect battery
  printf("\tConnect battery to central network");
  if(modbus_write_bit(plc[PLC_BATTERY], BATTERY_COIL_CONNECT_ADDR, enable) == -1)
    printf("\t%sERROR%s\n", KRED, KWHITE);
  else
    printf("\t%s%s%s\n", enable == ON ? KGREEN : KRED, enable == ON ? "ON" : "OFF", KWHITE);
}

uint16_t getBatteryCharge(modbus_t* plc[]) {
  uint16_t buffer[1];
  if(modbus_read_input_registers(plc[PLC_BATTERY], BATTERY_INPUT_CHARGE_ADDR, 1, buffer) == -1) {
    fprintf(stderr, "Error trying to get battery level\n");
    return 0;
  }
  return buffer[0];
}
