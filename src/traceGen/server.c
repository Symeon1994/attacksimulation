#include <stdlib.h>
#include <stdio.h>
#include <modbus.h>
#include <unistd.h>
#include <fcntl.h>
#include "server.h"
#include "constant.h"
#include "powerplantplc.h"
#include "plc.h"

int main(int argc,char* argv[]) {

	char* ip_address; 
	int check_addr = -1;
	int port = -1;
	int c;
	opterr = 0;

	while((c = getopt(argc,argv,"i:p:")) != -1){
		switch(c){
			case 'i':
				ip_address = optarg;
				check_addr = 1;
				break;
			case 'p':
				port = atoi(optarg);
				break;
			case '?':
				if(optopt == 'i' || optopt == 'p')
					fprintf(stderr,"Option -%c requires an argument.\n",optopt);
				else
					fprintf(stderr,"Unknown option character %c\n",optopt);
			default:
				abort();
		}
	}
	if(port >0 && check_addr>0)
		launchPLC(ip_address,port);
	else
		fprintf(stderr,"missing required argument\n");
	return 0;
}

void launchPLC(char* ip_address,int port) {
  int pipefd[2*NB_PLC];
  int i = 0, j = 0;
  for(i = 0 ; i < NB_PLC ; i++) {
    // Error
    if(pipe(pipefd + 2*i) == -1) {
      fprintf(stderr, "Error creating pipes\n");
      for(j = 0 ; j < i ; i++)
        close(pipefd[i]);
      exit(-1);
    }
    else {
      // Non blocking read op
      fcntl(pipefd[2*i], F_SETFL, O_NONBLOCK);
      fcntl(pipefd[2*i+1], F_SETFL, O_NONBLOCK);
    }
  }

  // Launching plc processes
  plc(NULL, pipefd, PLC_COAL_DOOR,ip_address,port);
  plc(pipefd, pipefd+2, PLC_COAL_BRINGER,ip_address,port);
  plc(pipefd+2, pipefd+4, PLC_FURNACE,ip_address,port);
  plc(pipefd+4, pipefd+6, PLC_WATER,ip_address,port);
  plc(pipefd+6, pipefd+8, PLC_TURBINE,ip_address,port);
  plc(pipefd+8, NULL, PLC_BATTERY,ip_address,port);

  // closing pipes
  for(i = 0 ; i < NB_PLC*2 ; i++)
    close(pipefd[i]);
}
