#include <stdlib.h>
#include <stdio.h>
#include <modbus.h>
#include <unistd.h>
#include "modbusMaster.h"
#include "constant.h"

int main(int argc,char* argv[]) {
  char* ip_address; 
  int port = -1;
  int h_port = -1;
  int check_addr = -1;
  int c;
  opterr = 0;

  while((c = getopt(argc,argv,"i:p:t:")) != -1){
  	switch(c){
		case 'i':
			ip_address = optarg;
			check_addr = 1;
			break;
		case 'p':
			port = atoi(optarg);
			break;
		case 't':
			h_port = atoi(optarg);
			break;
		case '?':
			if(optopt == 'c')
					fprintf(stderr,"Option -%c requires an argument.\n",optopt);
			else
					fprintf(stderr,"Unknown option character %c\n",optopt);
		default:
			abort();
	}
  }
  if(port >0 && check_addr>0)
  	openMasterServer(ip_address,port,h_port);
  else
	fprintf(stderr,"missing  required argument\n");
  return 0;
}
