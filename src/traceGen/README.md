# Library Needed

## libmodbus

To install download from the [libmodbus](http://libmodbus.org/) site or go into your debian package manager and download libmodbus-3.0.5 (the version we shall use).

Use `make` to compile files, and if the linkage are wrong run
```sh
pkg-config --cflags -libs libmodbus
```
and change the `INCLUDE` variables in the makefile

## Running

To run the simulation compile with  `make`, then you should first launch the PLC and then the master server.

```sh
./server
./master
```

Finally launch the HMI, and us the `help` command to get the list of the commands.
```sh
./hmi
> help
```
