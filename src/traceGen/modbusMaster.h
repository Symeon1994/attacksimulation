#ifndef MODBUS_MASTER_H
#define MODBUS_MASTER_H

#include <modbus.h>

#define ACT_EMPTY                   0
// Coaldrop
#define ACT_DROP_COAL               1
#define ACT_GET_DROPPED_AMOUNT      2
// Coal bringer
#define ACT_GET_COAL_AMOUNT         3
#define ACT_GET_CURRENT_RACK        4
#define ACT_ROTATE                  5
// Furnace
#define ACT_LIGHT_FURNACE           6
#define ACT_GET_FURNACE_TEMP        8
#define ACT_GET_FURNACE_COAL_AMOUNT 9
// Water
#define ACT_GET_WATER_LVL           10
#define ACT_GET_STEAM_LVL           11
#define ACT_GET_WATER_TEMP          12
#define ACT_WATER_FLOW_IN           13
#define ACT_WATER_FLOW_OUT          20
// Turbine
#define ACT_LIMIT_TURBINE_ROT       14
#define ACT_ENABLE_TURBINE          15
#define ACT_GET_TURBINE_ROT         16
// Battery
#define ACT_LIMIT_BATTERY_AMOUNT    17
#define ACT_CONNECT_BATTERY         18
#define ACT_GET_BATTERY_CHARGE      19

// Stack structure to store actions
#define ACTION_TYPE 0
struct action_t {
  unsigned int type; // Type of action
  void* params; // Array of parameters, null if none
  struct action_t* next; // Next action, null if none
};

// Constraint constant
#define ELEM_FURNACE_COAL     0
#define ELEM_FURNACE_TEMP     1
#define ELEM_WATER_AMOUNT     2
#define ELEM_STEAM_AMOUNT     3
#define ELEM_WATER_TEMP       4
#define ELEM_TURBINE_RPM      5
#define ELEM_BATTERY_CONTENT  6

// Priority
#define PRIORITY_COMPUTER 0
#define PRIORITY_USER 1

// Structure for the list of constraint
#define CONSTRAINT_TYPE 1
struct constraint_t {
  uint8_t priority; // Priority of the constraint, either USER/COMPUTER
  uint8_t elem; // Element of the constraint
  uint16_t value; // Value of the constraint
  unsigned int period; // Period in seconds (DEFAULT_CONSTRAINT_PERIOD)
  time_t lastCheck; // Last time we checked the constraint
  uint8_t satisfied; // TRUE OR FALSE
  struct constraint_t* next; // next constraint, null if none
};

typedef struct action_t action_t;
typedef struct constraint_t constraint_t;

#define ERROR_TYPE 2

/** Open the power plant master server */
void openMasterServer(const char* ip_address,int port,int h_port);

/** Look at the action stack and perform the operations that are on it */
void runningPowerPlant(modbus_t* plc[]);

/** Look at the constraint and add/remove action to respect them */
void constraintSatisfaction(modbus_t* plc[]);

/** Push a new action on the stack */
void pushOnStack(action_t* a);

/** Count the number of action of a certain type on the stack */
int countAction(unsigned int type);

/** HMI input reader */
void* hmi_inputreader_thread(void* port);

/** Get the constraint period from the element of the constraint*/
int getPeriodFromElement(int type);

/** Add constraint
 * CAREFUL, you must lock yourself before using it*/
void addConstraint(constraint_t* cst);

/** Add constraint that are linked to a certain constraint */
void addRecursiveConstraint(constraint_t* cst);

// Functions to allocate actions
action_t* allocAction();
void freeAction(action_t* a);

#endif
