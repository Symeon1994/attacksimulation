#ifndef POWER_PLANT_PLC_H
#define POWER_PLANT_PLC_H

#include <modbus.h>

/**
 * Execute the operation over a particular type of plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 * @param ctx the modbus context
 * @param map the modbus register map
 * @param rfd input file descriptor
 * @param wfd output file descriptor
 */
void getOp(int constant, modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);

/**
 * Get the coil register amount for a particular plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 * @return the amount of needed register
 */
int getCoil(int constant);

/**
 * Get the holding register amount for a particular plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 * @return the amount of needed register
 */
int getHolding(int constant);

/**
 * Get the input register amount for a particular plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 * @return the amount of needed register
 */
int getInput(int constant);

/**
 * Get the discrete register amount for a particular plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 * @return the amount of needed register
 */
int getDiscrete(int constant);

/**
 * Init the register values for a particular plc
 * @param constant the type of a plc in PLC_COAL_DOOR, PLC_COAL_BRINGER, PLC_FURNACE, PLC_WATER, PLC_TURBINE, PLC_BATTERY
 */
void init_map(modbus_mapping_t* map, int constant);

/* Operations for plcs */
void op_coaldoorplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_coalbringerplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_furnaceplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_waterplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_turbineplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);
void op_batteryplc(modbus_t* ctx, modbus_mapping_t* map, int rfd, int wfd);

// Alarm FUNCTION
void rotation_over(int signal);
void updateFurnace(int signal);
void updateWater(int signal);
void updateTurbine(int signal);
void updateBattery(int signal);

#endif
