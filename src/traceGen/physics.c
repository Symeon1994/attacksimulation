#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "constant.h"
#include "physics.h"

long getGeneratedPower(unsigned long* gram, int t) {
  double amount = *gram * BURN_COAL_PER_SECOND * (t/1000.0);
  *gram = (int)(*gram - amount);
  return (int)(amount * COAL_ENERGY_PRODUCTION);
}

long getFurnacePowerLoss(long furnacePower, int t) {
  return (long)(furnacePower * (LOSS_OF_POWER_PER_SECOND+FURNACE_POWER_TRANSFER) * (t/1000.0));
}

int getAmbientTemperature(long energy) {
  int res = (int)(energy/1006);
  return res < START_FURNACE_TEMP ? START_FURNACE_TEMP : res;
}

long getWaterPowerLoss(long power, int t) {
  return (long)(power * LOSS_OF_POWER_PER_SECOND * (t/1000.0));
}

long getPowerTransfer(long furnacePower, int t) {
  return (long)(furnacePower * FURNACE_POWER_TRANSFER * (t/1000.0));
}

int getWaterTemperatureFromPower(long power, int qte) {
  if(qte == 0)
    return 0;
  return (int)(power/(4.185 * qte));
}

int getSteamAmountProduced(long power, int qte, double steam, int t) {
  long vapor = (long)(100 * (qte+steam) * 4.185);
  if(power < vapor)
    return 0;
  int steamPower = getEnergyForSteam(steam);
  if(power < steamPower + vapor)
    return 0;
  // Divide energy by 1000 to have kJ instead of J
  return (t/1000.0) * (power-(vapor+steamPower))/2264.76;
}

int getEnergyProduction(int turbineRPM, int delta) {
  return (int)(delta/1000.0 * turbineRPM * ENERGY_ROTATION_COE);
}

int getRPMFromQte(double qte) {
  return (int)(qte  * STEAM_ROTATION_COE);
}


int getRPMIncreaseFromQte(double qte, int delta, int rpm) {
  int variation = getRPMFromQte(qte) - rpm;
  double res = variation * delta/1000.0 * ROTATION_FRICTION;
  if(res == 0)
    return 0;
  else if(res > 0)
    return (int)ceil(res);
  if((int)floor(res) + rpm <= 0)
    return 0;
  return (int)floor(res);
}

int getTemperatureNeeded(double pressure) {
  if(pressure <= 0)
    return 0;
  return (int)(100.0*sqrt(pressure));
}

int getSteamOuput(int steamLevel, int time) {
  int out = (int)(STEAM_OUTPUT_PER_SECOND * steamLevel * time/1000.0);
  if(out > steamLevel)
    return steamLevel;
  return out;
}

int getEnergyForSteam(double steam) {
  return (int)(steam * 2264.76);
}

int constrant_getRPMFromEnergyProduction(int amount) {
  return (int)(amount/ENERGY_ROTATION_COE);
}

int constraint_getSteamAmountFromTurbineRPM(int rpm) {
  double steamInput = rpm/STEAM_ROTATION_COE;
  return (int)(steamInput/STEAM_OUTPUT_PER_SECOND);
}

int constraint_getTemperatureFromSteam(int steam, int qte) {
  if(steam == 0 || qte == 0)
    return 0;
  double energyToVapor = 100*qte*4.185;
  double energyInTheCuve = (int)(energyToVapor + steam * 2264.76);
  return getWaterTemperatureFromPower(energyInTheCuve, qte);
}

int constraint_getTemperatureFromSteamOutput(int steam, int qte) {
  if(steam == 0 || qte == 0)
    return START_WATER_TEMP;
  double energyToVapor = 100*qte*4.185;
  int steamInTheCuve = (int)(steam/STEAM_OUTPUT_PER_SECOND);
  double energyInTheCuve = (int)(energyToVapor + steamInTheCuve * 2264.76);
  return getWaterTemperatureFromPower(energyInTheCuve, qte);
}

int constraint_getFurnaceTempFromWaterTemp(int qte, int temp) {
  double energy = temp * qte * 4.185;
  return (int)(energy/1006);
}

int constraint_getCoalFromFurnaceTemperature(int temp) {
  // Furnace energy for this temperature
  double energy = temp * 1.006;
  // Lost in furnace
  double furnaceLoss = LOSS_OF_POWER_PER_SECOND + FURNACE_POWER_TRANSFER;
  // Production should compensate the loss
  double production = furnaceLoss * energy;
  return (int)((production/COAL_ENERGY_PRODUCTION)/BURN_COAL_PER_SECOND);
}
