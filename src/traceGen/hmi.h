#ifndef HMI_H
#define HMI_H

#define MSG_LEN 4

void printhelp();
void parseConstraint(char* buffer, char out[MSG_LEN]);
unsigned int parseValue(char* buffer);
void printStatus(int sock);

// Coal door function
uint16_t hmi_getTotalAmountDropped(int sock);
void hmi_dropCoal(int sock, uint16_t amount);

// Coal bringer function
uint16_t hmi_getCoalAmount(int sock, int i);
uint16_t hmi_getCurrentRack(int sock);
void hmi_rotateBringer(int sock);

// Furnace function
void hmi_lightFurnace(int sock);
uint16_t hmi_getFurnaceTemperature(int sock);
uint16_t hmi_getFurnaceCoalAmount(int sock);

// Water function
uint16_t hmi_getWaterLevel(int sock);
uint16_t hmi_getSteamLevel(int sock);
uint16_t hmi_getWaterTemperature(int sock);
void hmi_waterFlowIn(int sock, uint16_t amount);
void hmi_waterFlowOut(int sock, uint16_t amount);

// Turbine function
void hmi_limitTurbineRotation(int sock, uint16_t limit);
void hmi_enableTurbine(int sock, uint8_t enable);
uint16_t hmi_getTurbineRotation(int sock);

// Battery function
void hmi_limitBatteryContent(int sock, uint16_t limit);
void hmi_connectBattery(int sock, uint8_t connection);
uint16_t hmi_getBatteryCharge(int sock);


#endif
