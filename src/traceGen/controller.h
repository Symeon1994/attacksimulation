#ifndef CONTROLLER_H
#define CONTROLLER_H

#define KWHITE "\x1B[0m"
#define KRED   "\x1B[31m"
#define KGREEN "\x1B[32m"

/*** Coal door function ***/

/** @return the total amount of coal dropped since the start of the simulation*/
int getTotalAmountDropped(modbus_t* plc[]);

/**
 * Drop coal on the current rack of the coal bringer
 * @param amount the coal to be dropped, one rack is limited by MAX_COAL_PER_BRINGER_RACK
 */
void dropCoal(modbus_t* plc[], uint16_t amount);

/*** Coal bringer function ***/

/**
 * @param i the rack number in [0;TOTAL_RACK_NUMBER-1]
 * @return the amount of coal on the asked rack
 */
uint16_t getCoalAmount(modbus_t* plc[], int i);

/** @return get the current rack, under the machine that drop the coal*/
uint16_t getCurrentRack(modbus_t* plc[]);

/** Rotate the bringer and change the current rack */
void rotateBringer(modbus_t* plc[]);

/*** Furnace function ***/
void lightFurnace(modbus_t* plc[]);
uint16_t getFurnaceTemperature(modbus_t* plc[]);
uint16_t getFurnaceCoalAmount(modbus_t* plc[]);

/*** Water function ***/
uint16_t getWaterLevel(modbus_t* plc[]);
uint16_t getSteamLevel(modbus_t* plc[]);
uint16_t getWaterTemperature(modbus_t* plc[]);
void flowWaterIn(modbus_t* plc[], uint16_t amount);
void flowWaterOut(modbus_t* plc[], uint16_t amount);

/*** Turbine function ***/
void limitTurbineRotation(modbus_t* plc[], uint16_t limit);
void enableTurbine(modbus_t* plc[], uint8_t enable);
uint16_t getTurbineRotation(modbus_t* plc[]);

/*** Battery function ***/
void limitBatteryContent(modbus_t* plc[], uint16_t limit);
void connectBattery(modbus_t* plc[], uint8_t connection);
uint16_t getBatteryCharge(modbus_t* plc[]);

#endif
