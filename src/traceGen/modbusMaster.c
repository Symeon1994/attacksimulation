#include <stdlib.h>
#include <stdio.h>
#include <modbus.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "constant.h"
#include "physics.h"
#include "controller.h"
#include "modbusUtils.h"
#include "modbusMaster.h"

action_t* _actionStack = NULL;
constraint_t* _constraintList = NULL;
pthread_mutex_t _actionStackMutex;
pthread_mutex_t _constraintListMutex;
modbus_t* _plc[NB_PLC];

void openMasterServer(const char* ip_address,int port, int h_port) {
  int i;

  struct timeval response_timeout;
  response_timeout.tv_sec = 5;
  response_timeout.tv_usec= 0;

  // Wait for NB_PLS connection on socket
  for(i = 0 ; i < NB_PLC ; i++) {
    //_plc[i] = openServer(IP_ADDRESS, PORT+i);
    _plc[i] = openServer(ip_address, port+i);
    modbus_set_debug(_plc[i], FALSE);
    modbus_connect(_plc[i]);
    // Timeout
    modbus_set_response_timeout(_plc[i], &response_timeout);
    // TODO GESTION PLANTAGE Close socket dans le thread et fermer le thread
  }

  // Allocating mutex for the stack and the list
  if(pthread_mutex_init(&_actionStackMutex, NULL) != 0) {
    // Closing
    for(i = 0 ; i < NB_PLC ; i++)
      closeServer(_plc[i]);
    // Error
    fprintf(stderr, "Error initializing the action mutex\n");
    exit(-1);
  }
  if(pthread_mutex_init(&_constraintListMutex, NULL) != 0) {
    // Closing
    pthread_mutex_destroy(&_actionStackMutex);
    for(i = 0 ; i < NB_PLC ; i++)
      closeServer(_plc[i]);
    // Error
    fprintf(stderr, "Error initializing the constraint mutex\n");
    exit(-1);
  }

  // Launch HMI thread
  pthread_t thread; // TODO
  if(pthread_create(&thread, NULL, &(hmi_inputreader_thread),&h_port) != 0) {
    // Freeing
    pthread_mutex_destroy(&_actionStackMutex);
    for(i = 0 ; i < NB_PLC ; i++)
      closeServer(_plc[i]);
    // Error
    fprintf(stderr, "Error creating thread\n");
    exit(-1);
  }

  // Executing actions
  while(TRUE) {
    runningPowerPlant(_plc);
    constraintSatisfaction(_plc);
  }

  // Freeing
  pthread_mutex_destroy(&_actionStackMutex);
  pthread_mutex_destroy(&_constraintListMutex);
  freeAction(_actionStack);
  while(_constraintList != NULL) {
    constraint_t* tmp = _constraintList;
    _constraintList = tmp->next;
    free(tmp);
  }
  for(i = 0 ; i < NB_PLC ; i++)
    closeServer(_plc[i]);
}

action_t* allocAction() {
  action_t* res = (action_t*)malloc(sizeof(action_t));
  if(res == NULL) {
    fprintf(stderr, "Error trying to allocate action stack!\n");
    return NULL;
  }
  res->type = ACT_EMPTY;
  res->params = NULL;
  res->next = NULL;
  return res;
}

void freeAction(action_t* a) {
  while(a != NULL) {
    action_t* tmp = a;
    a = tmp->next;
    if(tmp->params != NULL)
      free(tmp->params);
    free(tmp);
  }
}

void runningPowerPlant(modbus_t* _plc[]) {
  pthread_mutex_lock(&_actionStackMutex);
  action_t* top = _actionStack;
  // If stack not empty, pop first element
  if(top != NULL) {
    _actionStack = top->next;
    top->next = NULL;
  }
  pthread_mutex_unlock(&_actionStackMutex);

  // Perform the action
  if(top != NULL) {
    switch(top->type) {
      case ACT_DROP_COAL: dropCoal(_plc, ((uint16_t*)top->params)[0]); break;
      case ACT_ROTATE: rotateBringer(_plc); break;
      case ACT_LIGHT_FURNACE: lightFurnace(_plc); break;
      case ACT_WATER_FLOW_IN: flowWaterIn(_plc, ((uint8_t*)top->params)[0]); break;
      case ACT_WATER_FLOW_OUT: flowWaterOut(_plc, ((uint8_t*)top->params)[0]); break;
      case ACT_LIMIT_TURBINE_ROT: limitTurbineRotation(_plc, ((uint16_t*)top->params)[0]); break;
      case ACT_ENABLE_TURBINE: enableTurbine(_plc, ((uint8_t*)top->params)[0]); break;
      case ACT_LIMIT_BATTERY_AMOUNT: limitBatteryContent(_plc, ((uint16_t*)top->params)[0]); break;
      case ACT_CONNECT_BATTERY: connectBattery(_plc, ((uint8_t*)top->params)[0]); break;
      // Sleep 100 ms TODO Constant
      case ACT_EMPTY: usleep(100 * 1000); break;
      default: break;
    }
    // Free the poped element
    freeAction(top);
  }
}

/**
 * The constraint are recursive, meaning that if the battery content is not
 * correct according to the constraint, a new constraint will be generated setting
 * the turbine RPM. If a constraint for the turbine RPM already exist, the
 * constraint will be greater.
 * FURNACE COAL
 *      ^
 *      |
 * FURNACE TEMP
 *      ^
 *      |
 * WATER TEMP ---> Water Level
 *      ^
 *      |
 * TURBINE RPM
 *      ^
 *      |
 * BATTERY CONTENT
 */
void constraintSatisfaction(modbus_t* _plc[]) {
  pthread_mutex_lock(&_constraintListMutex);
  constraint_t* current = _constraintList;
  time_t cur = time(NULL);
  while(current != NULL) {
    if(difftime(cur, current->lastCheck) > current->period) {
      current->lastCheck = cur;
      uint16_t value;
      switch(current->elem) {
        case ELEM_FURNACE_COAL:
          value = getFurnaceCoalAmount(_plc);
          // Too much coal, burn some
          if(value > current->value) {
            current->satisfied = FALSE;
            action_t* lightFurnace = allocAction();
            lightFurnace->type = ACT_LIGHT_FURNACE;
            pushOnStack(lightFurnace);
          }
          // Not enough coal, drop some
          else if(value < current->value) {
            current->satisfied = FALSE;
            // Max 5 Drop and rotate on the stack TODO REMOVE BECAUSE OF PERIOD
            if(countAction(ACT_DROP_COAL) < 1+(current->value-value)/MAX_COAL_PER_BRINGER_RACK) {
              // Drop Coal
              uint16_t amount = current->value - value;
              action_t* drop = allocAction();
              drop->type = ACT_DROP_COAL;
              drop->params = malloc(sizeof(uint16_t));
              ((uint16_t*)drop->params)[0] = amount > MAX_COAL_PER_BRINGER_RACK ? MAX_COAL_PER_BRINGER_RACK : amount;
              pushOnStack(drop);
              // Rotate
              action_t* rotate = allocAction();
              rotate->type = ACT_ROTATE;
              pushOnStack(rotate);
            }
          }
          else
            current->satisfied = TRUE; // TODO current->satisfied < 0 ? current->satisfied-1 : -1;
          break;
        case ELEM_WATER_AMOUNT:
          value = getWaterLevel(_plc);
          // Too much or not enough water
          if(value > current->value || value < current->value) {
            current->satisfied = FALSE;
            int amount = current->value - value;
            // Flow speed
            int tmpval = amount < 0 ? -amount : amount;
            tmpval = tmpval/CONSTRAINT_WATER_LEVEL_PERIOD;
            while(tmpval > WATER_MAX_FLOW)
              tmpval = tmpval/CONSTRAINT_WATER_LEVEL_PERIOD;

            action_t* waterflow = allocAction();
            waterflow->type = amount < 0 ? ACT_WATER_FLOW_OUT : ACT_WATER_FLOW_IN;
            waterflow->params = malloc(sizeof(uint16_t));
            tmpval = (100*tmpval/WATER_MAX_FLOW);
            ((uint16_t*)waterflow->params)[0] = tmpval > 100 ? 100 : tmpval;
            pushOnStack(waterflow);
          }
          // Its good
          else {
            // if we just succeed to satisfy the constraint
            if(current->satisfied == FALSE) {
              // Stop in and out water flow
              action_t* waterflow = allocAction();
              waterflow->type = ACT_WATER_FLOW_OUT;
              waterflow->params = malloc(sizeof(uint16_t));
              ((uint16_t*)waterflow->params)[0] = 0;
              pushOnStack(waterflow);
              waterflow = allocAction();
              waterflow->type = ACT_WATER_FLOW_IN;
              waterflow->params = malloc(sizeof(uint16_t));
              ((uint16_t*)waterflow->params)[0] = 0;
              pushOnStack(waterflow);
            }
            current->satisfied = TRUE;
          }
          break;
        case ELEM_FURNACE_TEMP:
          value = getFurnaceTemperature(_plc);
          if(value == current->value)
            current->satisfied = TRUE;
          else
            current->satisfied = FALSE;
          break;
        case ELEM_STEAM_AMOUNT:
          value = getSteamLevel(_plc);
          if(value == current->value)
            current->satisfied = TRUE;
          else
            current->satisfied = FALSE;
          break;
        case ELEM_WATER_TEMP:
          value = getWaterTemperature(_plc);
          if(value == current->value)
            current->satisfied = TRUE;
          else
            current->satisfied = FALSE;
          break;
        case ELEM_TURBINE_RPM:
          value = getTurbineRotation(_plc);
          if(value == current->value)
            current->satisfied = TRUE;
          else
            current->satisfied = FALSE;
          break;
        case ELEM_BATTERY_CONTENT:
          value = getBatteryCharge(_plc);
          if(value == current->value)
            current->satisfied = TRUE;
          else
            current->satisfied = FALSE;
          break;
        default:
          printf("Unknown constraint element\n");
          break;
      }
    }
    // Next
    current = current->next;
  }
  pthread_mutex_unlock(&_constraintListMutex);
}

// TODO RENAME AS THIS FUNCTION MAKES IT A FIFO LIST
void pushOnStack(action_t* a) {
  pthread_mutex_lock(&_actionStackMutex);
  if(_actionStack == NULL) {
    _actionStack = a;
  }
  else {
    action_t* cur = _actionStack;
    while(cur->next != NULL) {
        cur = cur->next;
    }
    cur->next = a;
  }
  pthread_mutex_unlock(&_actionStackMutex);
}

int countAction(unsigned int type) {
  pthread_mutex_lock(&_actionStackMutex);
  if(_actionStack == NULL) {
    pthread_mutex_unlock(&_actionStackMutex);
    return 0;
  }
  action_t* cur = _actionStack;
  int res = 0;
  while(cur != NULL) {
      if(cur->type == type)
        res++;
      cur = cur->next;
  }
  pthread_mutex_unlock(&_actionStackMutex);
  return res;
}

void* hmi_inputreader_thread(void* port) {
  int n;
  int sockfd,newsockfd;
  socklen_t hmilen;
  struct sockaddr_in master_addr,hmi_addr;
  char input[4];

  //Socket creation
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0){
      printf("Could not create socket");
      exit(-1);
  }
  bzero((char*) &master_addr, sizeof(master_addr));
  master_addr.sin_family = AF_INET;
  //master_addr.sin_port = htons(MASTERPORT);
  int * p_port = (int*) port;
  fprintf(stderr,"MasterPort: %d",*p_port);
  master_addr.sin_port = htons(*p_port);
  if (bind(sockfd, (struct sockaddr *) &master_addr, sizeof(master_addr))<0){
       printf("Could not bind socket");
       exit(-1);
  }
  listen(sockfd,2);
  hmilen = sizeof(hmi_addr);
  newsockfd = accept(sockfd,(struct sockaddr *) &hmi_addr,&hmilen);
  if(newsockfd<0){
       printf("Error on accept\n");
       exit(-1);
  }
  printf("Connect to the HMI\n");

  // Main loop
  while(TRUE) {
    bzero(input,4);
    n = read(newsockfd,input,4);
    if(n < 0){
        printf("Error on read");
        close(sockfd);
        exit(-1); // TODO PROPER DELETE ALL VARIABLES FROM THE SOFTWARE
    } else if(n != 4) {
      printf("Error on TCP read, exiting");
      close(sockfd);
      exit(-1); // TODO DELETE ALL VARIABLES FROM THE SOFTWARE
    }
    // Add actions
    action_t* action = (action_t*)malloc(sizeof(action_t));
    uint8_t* array = NULL;
    action->type = (unsigned int)input[1];
    uint16_t resp = 0;
    // Received action
    if(input[0] == ACTION_TYPE) {
      switch(action->type) {
        // Action without parameters
        case ACT_ROTATE:
        case ACT_LIGHT_FURNACE:
          action->params = NULL;
          break;
        // Action with uint8_t params
        case ACT_ENABLE_TURBINE:
        case ACT_CONNECT_BATTERY:
          array = (uint8_t*)malloc(sizeof(uint8_t));
          array[0] = (uint8_t)input[3];
          action->params = (void*)array;
          break;
        // Action with uint16_t params
        case ACT_DROP_COAL:
        case ACT_WATER_FLOW_IN:
        case ACT_WATER_FLOW_OUT:
        case ACT_LIMIT_TURBINE_ROT:
        case ACT_LIMIT_BATTERY_AMOUNT:
          array = (uint8_t*)malloc(sizeof(uint16_t));
          array[0] = (uint16_t)input[3];
          action->params = (void*)array;
          printf("%d\n", ((uint16_t*)action->params)[0]); // FIXME COAL RECEIVED AFTER START
          break;
        case ACT_GET_DROPPED_AMOUNT:
          resp = getTotalAmountDropped(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_COAL_AMOUNT:
          printf("TODO GET COAL AMOUT\n");
          free(action);
          action = NULL;
          break;
        case ACT_GET_CURRENT_RACK:
          resp = getCurrentRack(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_FURNACE_TEMP:
          resp = getFurnaceTemperature(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_FURNACE_COAL_AMOUNT:
          resp = getFurnaceCoalAmount(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_WATER_LVL:
          resp = getWaterLevel(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_STEAM_LVL:
          resp = getSteamLevel(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_WATER_TEMP:
          resp = getWaterTemperature(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_TURBINE_ROT:
          resp = getTurbineRotation(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        case ACT_GET_BATTERY_CHARGE:
          resp = getBatteryCharge(_plc);
          write(newsockfd, (char*)(&resp), 2);
          free(action);
          action = NULL;
          break;
        default:
          printf("HMI did unknown function call: %d\n", action->type);
          free(action);
          action = NULL;
          break;
      }
      if(action != NULL) {
        pushOnStack(action);
      }
    }
    // Received constraint
    else if(input[0] == CONSTRAINT_TYPE) {
      printf("Received constraint from HMI\n");
      constraint_t* c = (constraint_t*)malloc(sizeof(constraint_t));
      c->priority = PRIORITY_USER;
      c->elem = input[1];
      c->value = (((uint16_t)input[2]) << 8) | (((uint16_t)input[3]) & 0xFF);
      c->next = NULL;
      c->period = getPeriodFromElement(c->elem);
      c->lastCheck = time(NULL);
      c->satisfied = 0;
      pthread_mutex_lock(&_constraintListMutex);
      addConstraint(c);
      addRecursiveConstraint(c);
      pthread_mutex_unlock(&_constraintListMutex);
    }
  }
  close(newsockfd);
  close(sockfd);
  return NULL;
}

int getPeriodFromElement(int type) {
    switch(type) {
      case ELEM_FURNACE_COAL: return CONSTRAINT_FURNACE_COAL_PERIOD;
      case ELEM_FURNACE_TEMP: return CONSTRAINT_FURNACE_TEMP_PERIOD;
      case ELEM_WATER_AMOUNT: return CONSTRAINT_WATER_LEVEL_PERIOD;
      case ELEM_STEAM_AMOUNT: return CONSTRAINT_STEAM_AMOUNT_PERIOD;
      case ELEM_WATER_TEMP: return CONSTRAINT_WATER_TEMP_PERIOD;
      case ELEM_TURBINE_RPM: return CONSTRAINT_TURBINE_ROTATION_PERIOD;
      case ELEM_BATTERY_CONTENT: return CONSTRAINT_BATTERY_CONTENT_PERIOD;
      default: return 1;
    }

}

void addConstraint(constraint_t* cst) {
  // If constraint are opposed, the most recent is priviligied
  if(_constraintList == NULL) {
    _constraintList = cst;
  }
  // If the top of the list is opposed
  else if(cst->elem == _constraintList->elem) {
    constraint_t* cur = _constraintList;
    _constraintList = _constraintList->next;
    cur->next = NULL;
    free(cur);
    // Recursive call
    addConstraint(cst);
  }
  else {
    constraint_t* previous = _constraintList;
    constraint_t* cur = _constraintList->next;
    while(cur != NULL) { // TODO check the add to see if it's good
      if(cst->elem == cur->elem) {
        constraint_t* todel = cur;
        cur = cur->next;
        previous->next = cur;
        todel->next = NULL;
        free(todel);
      }
      else {
        previous =  cur;
        cur = cur->next;
      }
    }
    cst->next = _constraintList;
    _constraintList = cst;
  }
}

void addRecursiveConstraint(constraint_t* cst) {
  constraint_t* c = (constraint_t*)malloc(sizeof(constraint_t));
  c->priority = PRIORITY_COMPUTER;
  c->next = NULL;
  c->lastCheck = time(NULL);
  c->satisfied = 0;

  switch(cst->elem) {
    // Need for other cosntraint, create, add and recursively call the function
    case ELEM_FURNACE_TEMP:
      // We need to generate furnace coal constraint
      c->elem = ELEM_FURNACE_COAL;
      c->value = constraint_getCoalFromFurnaceTemperature(cst->value);
      c->period = getPeriodFromElement(ELEM_FURNACE_COAL);
      addConstraint(c);
      addRecursiveConstraint(c);
      break;
    case ELEM_STEAM_AMOUNT:
      // We need to generate water temperature constraint, considering that
      // the amount of water will be the DEFAULT_WATER_LEVEL
      c->elem = ELEM_WATER_TEMP;
      c->value = constraint_getTemperatureFromSteam(cst->value, DEFAULT_WATER_LEVEL);
      c->period = getPeriodFromElement(ELEM_WATER_TEMP);
      addConstraint(c);
      // TODO recursive call on elem water amount will generate constraint on water
      // If some constraint exists, do we keep it?
      addRecursiveConstraint(c);
      break;
    case ELEM_WATER_TEMP:
      // We need to generate the water amount
      c->elem = ELEM_WATER_AMOUNT;
      c->value = DEFAULT_WATER_LEVEL;
      c->period = getPeriodFromElement(ELEM_WATER_AMOUNT);
      addConstraint(c);
      addRecursiveConstraint(c);
      // We need to generate the furnace temperature constraint
      constraint_t* c2 = (constraint_t*)malloc(sizeof(constraint_t));
      c2->priority = PRIORITY_COMPUTER;
      c2->next = NULL;
      c2->lastCheck = time(NULL);
      c2->satisfied = 0;
      c2->elem = ELEM_FURNACE_TEMP;
      c2->value = constraint_getFurnaceTempFromWaterTemp(DEFAULT_WATER_LEVEL, cst->value);
      c2->period = getPeriodFromElement(ELEM_FURNACE_TEMP);
      addConstraint(c2);
      addRecursiveConstraint(c2);
      break;
    case ELEM_TURBINE_RPM:
      // We need to generate steam amount constraint
      c->elem = ELEM_STEAM_AMOUNT;
      c->value = constraint_getSteamAmountFromTurbineRPM(cst->value); // TODO SET LIMIT
      c->period = getPeriodFromElement(ELEM_STEAM_AMOUNT); // TODO VERIF
      addConstraint(c);
      addRecursiveConstraint(c);
      break;
    case ELEM_BATTERY_CONTENT:
      printf("Received battery content constraint\n"); // TODO remove
      // We need to generate turbine rpm constraint
      c->elem = ELEM_TURBINE_RPM;
      c->value = constrant_getRPMFromEnergyProduction(cst->value); // TODO SET LIMIT
      c->period = getPeriodFromElement(ELEM_TURBINE_RPM); // TODO VERIF
      addConstraint(c);
      addRecursiveConstraint(c);
      break;
    // No recursive add needed
    case ELEM_FURNACE_COAL: // TODO update constraint that are linked to coal, move in add constraint
    case ELEM_WATER_AMOUNT: // TODO UPDATE CONSTRAINT THAT ARE linked to water
    default:
      free(c);
      break;
  }
}
