#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "constant.h"
#include "modbusMaster.h"
#include "hmi.h"

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc,char *argv[])
{
    int sockfd;
    struct sockaddr_in master_addr,hmi_addr;
    //struct hostent *master;
    char buffer[256];

    /* Output array (4 bytes - MSG_LEN):
     * -> Constraint
     * +----------+----------+---------------------+
     * | CONS/ACT | ELEMENT  |       VALUE         |
     * +----------+----------+---------------------+
     * |  8 bits  |  8 bits  |       16 bits       |
     * +----------+----------+---------------------+
     *
     * -> Action
     * +----------+----------+---------------------+
     * | CONS/ACT |  ACTION  |        VALUE        |
     * +----------+----------+---------------------+
     * |  8 bits  |  8 bits  |      16 bits        |
     * +----------+----------+---------------------+
     */

	char* m_ip_addr;
	//char* h_ip_addr;
	int m_port = -1;
	int h_port = -1;
	int m_check_addr = -1;
	int h_check_addr = -1;
	int c;
	opterr = 0;

	while((c = getopt(argc,argv,"i:p:a:t:")) != -1){
		switch(c){
			case 'i':
				m_ip_addr = optarg;
				m_check_addr = 1;
				break;
			case 'p':	
				m_port = atoi(optarg);
				break;
			case 'a':
				//h_ip_addr = optarg;
				h_check_addr = 1;
				break;
			case 't':
				h_port = atoi(optarg);
				break;
			case '?':
				fprintf(stderr,"-i\tmaster ip address\n-p\tmaster port\n-a\thmi ip address\n-t\thmi port\n");	
				return 1;
			default:
				abort();
		}

	}

	if(m_check_addr < 0 && h_check_addr < 0 && m_port <0 && h_port <0){
		fprintf(stderr,"missing required argument\n");
		return 0;
	}
	

    char out[MSG_LEN];
    sockfd = socket(AF_INET,SOCK_STREAM,0);
    if(sockfd < 0){
         error("ERROR opening socket\n");
    }


    bzero((char*) &hmi_addr, sizeof(hmi_addr));
    hmi_addr.sin_family = AF_INET;
    //hmi_addr.sin_port = htons(HMIPORT);
    hmi_addr.sin_port = htons(h_port);
    if (bind(sockfd, (struct sockaddr *) &hmi_addr, sizeof(hmi_addr))<0){
       error("Could not bind socket");
    }

    //master = gethostbyname("localhost");
    //if(master== NULL) {
    //     error("ERROR no such host\n");
    //}
    bzero((char *) &master_addr, sizeof(master_addr));
    master_addr.sin_family = AF_INET;
	inet_aton(m_ip_addr,&master_addr.sin_addr);
    //bcopy((char*) master->h_addr,
    //        (char*)&master_addr.sin_addr.s_addr,
    //        master->h_length);
	//bcopy(m_ip_addr,(char*)&master_addr.sin_addr.s_addr,16);
    //master_addr.sin_port = htons(MASTERPORT);
    master_addr.sin_port = htons(m_port);
    if (connect(sockfd,(struct sockaddr*) &master_addr,sizeof(master_addr))<0){
         error("connecting");
    }

    // Main loop
    while(TRUE) {
      printf("> ");
      bzero(buffer,256);
      bzero(out, MSG_LEN);
      fgets(buffer,255,stdin);
      if(strncmp(buffer, "help", 4) == 0)
        printhelp();
      else if(strncmp(buffer, "start", 5) == 0) {
        hmi_enableTurbine(sockfd, ON);
        hmi_connectBattery(sockfd, ON);
        hmi_limitTurbineRotation(sockfd, 100);
        hmi_limitBatteryContent(sockfd, 100);
        out[0] = (char)CONSTRAINT_TYPE;
        out[1] = (char)ELEM_BATTERY_CONTENT;
        out[2] = (uint8_t)((20 & 0xff00) >> 8);
        out[3] = (uint8_t)(20 & 0xff);
        write(sockfd, out, MSG_LEN);
      }
      // Get
      else if(strncmp(buffer, "get dropped amount", 18) == 0) {
        printf("> %d\n", hmi_getTotalAmountDropped(sockfd));
      }
      else if(strncmp(buffer, "get current rack", 16) == 0) {
        printf("> %d\n", hmi_getCurrentRack(sockfd));
      }
      else if(strncmp(buffer, "get furnace temperature", 23) == 0) {
        printf("> %d°C\n", hmi_getFurnaceTemperature(sockfd));
      }
      else if(strncmp(buffer, "get furnace coal", 16) == 0) {
        printf("> %d\n", hmi_getFurnaceCoalAmount(sockfd));
      }
      else if(strncmp(buffer, "get cuve water", 14) == 0) {
        printf("> %d\n", hmi_getWaterLevel(sockfd));
      }
      else if(strncmp(buffer, "get cuve steam", 14) == 0) {
        printf("> %d\n", hmi_getSteamLevel(sockfd));
      }
      else if(strncmp(buffer, "get cuve temperature", 20) == 0) {
        printf("> %d°C\n", hmi_getWaterTemperature(sockfd));
      }
      else if(strncmp(buffer, "get turbine rotation", 20) == 0) {
        printf("> %d%%\n", hmi_getTurbineRotation(sockfd));
      }
      else if(strncmp(buffer, "get battery charge", 18) == 0) {
        printf("> %d%%\n", hmi_getBatteryCharge(sockfd));
      }
      else if(strncmp(buffer, "get", 3) ==0) {
        printf("You have to specify options:\n\tdropped amount\n\tcurrent rack\n\tfurnace temperature\n\tfurnace coal\n\tcuve water\n\tcuve steam\n\tcuve temperature\n\tturbine rotation\n\tbattery charge\n\tstop\n\tconstraint_list\n");
      }
      // Set
      else if(strncmp(buffer, "drop", 4) == 0) {
        int val = atoi(buffer + 5);
        if(val <= 0)
          printf("You cannot drop nothing! Enter a valid amount!\n");
        else
          hmi_dropCoal(sockfd, (uint16_t)val);
      }
      else if(strncmp(buffer, "rotate", 6) == 0) {
        hmi_rotateBringer(sockfd);
      }
      else if(strncmp(buffer, "light furnace", 13) == 0) {
        hmi_lightFurnace(sockfd);
      }
      else if(strncmp(buffer, "limit", 5) == 0) {
        if(strncmp(buffer+6, "rotation", 8) == 0) {
          int val = atoi(buffer+ 15);
          if(val >= 0 && val <= 100) {
            hmi_limitTurbineRotation(sockfd, (uint16_t)val);
          }
          else {
            printf("Enter a valid value [0;100]\n");
          }
        } else if(strncmp(buffer+6, "battery", 7) == 0) {
          int val = atoi(buffer+ 14);
          if(val >= 0 && val <= 100) {
            hmi_limitBatteryContent(sockfd, (uint16_t)val);
          }
          else {
            printf("Enter a valid value [0;100]\n");
          }
        } else {
          printf("Non existing command! Type 'help' to get some help\n");
        }
      }
      else if(strncmp(buffer, "flow water", 10) == 0) {
        int val = atoi(buffer + 11);
        if(val < 0) {
          hmi_waterFlowOut(sockfd, -val);
        } else if(val > 0) {
          hmi_waterFlowIn(sockfd, val);
        } else {
          hmi_waterFlowIn(sockfd, 0);
          hmi_waterFlowOut(sockfd, 0);
        }
      }
      // Constraint
      else if(strncmp(buffer, "constraint", 10) == 0) {
        parseConstraint(buffer, out);
        if(out[0] == CONSTRAINT_TYPE) {
          write(sockfd, out, MSG_LEN);
        }
      }
      else if(strncmp(buffer, "stop", 4) == 0) {
        hmi_enableTurbine(sockfd, OFF);
        hmi_connectBattery(sockfd, OFF);
        hmi_limitTurbineRotation(sockfd, 0);
        hmi_limitBatteryContent(sockfd, 0);
        out[0] = (char)CONSTRAINT_TYPE;
        out[1] = (char)ELEM_BATTERY_CONTENT;
        out[2] = 0;// (uint8_t)((0 & 0xff00) >> 8);
        out[3] = 0;//(uint8_t)(0 & 0xff);
        write(sockfd, out, MSG_LEN);
      }
      else if(strncmp(buffer, "constraint_list", 15) == 0) {
        printf("Implement a way to show the list of constraint, extension to be done\n");
      }
      else if(strncmp(buffer, "quit", 4) == 0) {
        close(sockfd);
        return 0;
      }
      else if(strncmp(buffer, "status", 4) == 0) {
        printStatus(sockfd);
      }
      else {
        printf("Unknown command\n Type 'help' for help\n");
      }
    }
    close(sockfd);
    return 0;
}

void printhelp() {
  printf("Here are the different commands:\n");
  printf("\thelp\n\tstart\n\tquit\n\tconstraint <type> <integer>\n\tget <option>\n\tdrop <option>\n\trotate\n\tlight furnace\n\tlimit rotation <option>\n\tlimit battery <option>\n\tflow water -100/100\n\tstatus\n");
}

void printStatus(int sock) {
  printf("                               +-------------+\n");
  printf("                               |             |\n");
  printf("     __________          ______+______       |\n");
  printf("    /          \\        /  \\       /  \\      |\n");
  printf("   +    CUVE    +      +    \\     /    +     |\n");
  printf("   |            |      |     \\   /     |     |\n");
  printf("   |            |======|------%03d------|     |\n", hmi_getTurbineRotation(sock));
  printf("   |   % 5d S  |      |     /   \\     |     |\n", hmi_getSteamLevel(sock));
  printf("   |   % 5d L  |      +    /     \\    +     |\n", hmi_getWaterLevel(sock));
  printf("===|   % 5d°C  |===    \\__/_______\\__/      |\n", hmi_getWaterTemperature(sock));
  printf("   |            |           TURBINE          |\n");
  printf("   +------------+                            |\n");
  printf("  /              \\          +----------------+\n");
  printf(" /                \\         |\n");
  printf("+                  +     |  V  |\n");
  printf("|      FURNACE     |   +----+----+\n");
  printf("|                  |   |         |\n");
  printf("|     % 5d COAL   |   | BATTERY |\n", hmi_getFurnaceCoalAmount(sock));
  printf("|      % 5d°C     |   |   %03d%%  |\n", hmi_getFurnaceTemperature(sock), hmi_getBatteryCharge(sock));
  printf("|                  |   |         |\n");
  printf("+------------------+   +---------+\n");
}

/*
                               +-------------+
                               |             |
     __________          ______+______       |
    /          \        /  \       /  \      |
   +    CUVE    +      +    \     /    +     |
   |            |      |     \   /     |     |
   |            |======|------100------|     |
   |   2000 S   |      |     /   \     |     |
   |   1000 L   |      +    /     \    +     |
===|   9999°C   |===    \__/_______\__/      |
   |            |           TURBINE          |
   +------------+                            |
  /              \          +----------------+
 /                \         |
+                  +     |  V  |
|      FURNACE     |   +----+----+
|                  |   |         |
|     1200 COAL    |   | BATTERY |
|      5200°C      |   |   100%  |
|                  |   |         |
+------------------+   +---------+*/

void parseConstraint(char* buffer, char out[MSG_LEN]) {
  int i = 11; // strlen("constraint ")
  int recognizedConstraint = TRUE;
  out[0] = (char)CONSTRAINT_TYPE;

  // CONSTRAINT TYPE
  if(strncmp(buffer+i, "COAL", 4) == 0) {
    out[1] = (char)ELEM_FURNACE_COAL;
    i += 5;
  }
  else if(strncmp(buffer+i, "FURNACE_TEMP", 12) == 0) {
    out[1] = (char)ELEM_FURNACE_TEMP;
    i += 13;
  }
  else if(strncmp(buffer+i, "WATER_TEMP", 10) == 0) {
    out[1] = (char)ELEM_WATER_TEMP;
    i += 11;
  }
  else if(strncmp(buffer+i, "WATER", 5) == 0) {
    out[1] = (char)ELEM_WATER_AMOUNT;
    i += 6;
  }
  else if(strncmp(buffer+i, "STEAM", 5) == 0) {
    out[1] = (char)ELEM_STEAM_AMOUNT;
    i += 6;
  }
  else if(strncmp(buffer+i, "TURBINE", 7) == 0) {
    out[1] = (char)ELEM_TURBINE_RPM;
    i += 8;
  }
  else if(strncmp(buffer+i, "BATTERY", 7) == 0) {
    out[1] = (char)ELEM_BATTERY_CONTENT;
    i += 8;
  }
  else
    recognizedConstraint = FALSE;

  // CONSTRAINT VALUE
  if(recognizedConstraint == TRUE) {
    unsigned int val = parseValue(buffer+i);
    if(val == -1) {
      printf("Value is not an integer\n");
      out[0] = ERROR_TYPE;
    }
    else {
      out[2] = (uint8_t)((val & 0xff00) >> 8);
      out[3] = (uint8_t)(val & 0xff);
    }
  }
  else  {
    printf("Unknown constraint element\n\tCOAL\n\tFURNACE_TEMP\n\tWATER_TEMP\n\tWATER\n\tSTEAM\n\tTURBINE\n\tBATTERY\n");
    out[0] = ERROR_TYPE;
  }
}

unsigned int parseValue(char* buffer) {
  int i = 0;
  unsigned int res = 0;
  for(i = 0 ; buffer[i] != '\0' ; i++) {
    if(buffer[i] >= '0' && buffer[i] <= '9') {
      res *= 10;
      res += (buffer[i]-'0');
    }
    else if(i == 0)
      return -1;
    else
      return res;
  }
  return -1;
}

/*==================*
 * Command function *
 *==================*/

// Coal door function
uint16_t hmi_getTotalAmountDropped(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_DROPPED_AMOUNT;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

void hmi_dropCoal(int sock, uint16_t amount) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_DROP_COAL;
  out[2] = (uint8_t)((amount & 0xff00) >> 8);
  out[3] = (uint8_t)(amount & 0xff);
  write(sock, out, MSG_LEN);
}

// Coal bringer function
uint16_t hmi_getCoalAmount(int sock, int i) {
  printf("Getting the amount of coal for a particular rack has not been implemented, its an extension\n");
  return 0;
}

uint16_t hmi_getCurrentRack(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_CURRENT_RACK;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

void hmi_rotateBringer(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_ROTATE;
  write(sock, out, MSG_LEN);
}

// Furnace function
void hmi_lightFurnace(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_LIGHT_FURNACE;
  write(sock, out, MSG_LEN);
}

uint16_t hmi_getFurnaceTemperature(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_FURNACE_TEMP;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

uint16_t hmi_getFurnaceCoalAmount(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_FURNACE_COAL_AMOUNT;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

// Water function
uint16_t hmi_getWaterLevel(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_WATER_LVL;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

uint16_t hmi_getSteamLevel(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_STEAM_LVL;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

uint16_t hmi_getWaterTemperature(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_WATER_TEMP;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

void hmi_waterFlowIn(int sock, uint16_t amount) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_WATER_FLOW_IN;
  out[2] = (uint8_t)((amount & 0xff00) >> 8);
  out[3] = (uint8_t)(amount & 0xff);
  write(sock, out, MSG_LEN);
}

void hmi_waterFlowOut(int sock, uint16_t amount) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_WATER_FLOW_OUT;
  out[2] = (uint8_t)((amount & 0xff00) >> 8);
  out[3] = (uint8_t)(amount & 0xff);
  write(sock, out, MSG_LEN);
}

// Turbine function
void hmi_limitTurbineRotation(int sock, uint16_t limit) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_LIMIT_TURBINE_ROT;
  out[2] = 0x00;
  out[3] = (char)limit;
  write(sock, out, MSG_LEN);
}

void hmi_enableTurbine(int sock, uint8_t enable) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_ENABLE_TURBINE;
  out[2] = 0x00;
  out[3] = (char)enable;
  write(sock, out, MSG_LEN);
}

uint16_t hmi_getTurbineRotation(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_TURBINE_ROT;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}

// Battery function
void hmi_limitBatteryContent(int sock, uint16_t limit) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_LIMIT_BATTERY_AMOUNT;
  out[2] = 0x00;
  out[3] = (char)limit;
  write(sock, out, MSG_LEN);
}

void hmi_connectBattery(int sock, uint8_t connection) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_CONNECT_BATTERY;
  out[2] = 0x00;
  out[3] = (char)connection;
  write(sock, out, MSG_LEN);
}

uint16_t hmi_getBatteryCharge(int sock) {
  char out[MSG_LEN];
  bzero(out, MSG_LEN);
  out[0] = ACTION_TYPE;
  out[1] = (char)ACT_GET_BATTERY_CHARGE;
  write(sock, out, MSG_LEN);
  char in[2];
  read(sock, in, 2);
  return ((uint16_t*)in)[0];
}
