#ifndef PLC_H
#define PLC_H

/**
 * Create a new plc
 * @param in_pipe input pipe couple
 * @param out_pipe output pipe couple
 * @param constant the constant that will be added to the PORT to make it unique
 * @param reg the parameters for the modbus_mapping_t, the amount of register of the 4 types
 * @param op the function for operations;
 */
void plc(int in_pipe[2], int out_pipe[2], int constant,char* ip_address,int port);

#endif
