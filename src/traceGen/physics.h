#ifndef PHYSICS_H
#define PHYSICS_H

// The amount of energy produced by the coal in kJ/g
#define COAL_ENERGY_PRODUCTION 30
// The proportion of coal burned during one second in the furnace
#define BURN_COAL_PER_SECOND 0.002
// The loss of power in the furnace in proportion during one second
#define LOSS_OF_POWER_PER_SECOND 0.002
// The proportion of energy that transfer from the furnace to the cuve
#define FURNACE_POWER_TRANSFER 0.014
// The proportion of steam that output per second from the cuve to the turbine
#define STEAM_OUTPUT_PER_SECOND 0.5
// The part of RPM that transform into watt in the battery
#define ENERGY_ROTATION_COE 1
// The part of steam that will transform in RPM in the turbine
#define STEAM_ROTATION_COE 0.25
// Variation of 1 per second of rotation speed
#define ROTATION_FRICTION 1

/**
 * Get the generated power from a certain quantity of coal during a certain
 * period of time. BURN_COAL_PER_SECOND of the avaibable coal will be burn.
 * The coal produce COAL_ENERGY_PRODUCTION kJ/kg
 * @param gram a pointer to the amount a coal available in the furnace, this value
 * will be reduced by the amount of burned power
 * @param time the time elapsed in millisecond
 * @return the amount of generated power
 */
long getGeneratedPower(unsigned long* gram, int time);

/**
 * The loss of power inside of the furnace for a certain period of time
 * The loss will be of FURNACE_LOSS_OF_POWER_PER_SECOND+FURNACE_POWER_TRANSFER of the energy
 * @param furnacePower the power inside of the furnace
 * @param time the time elapsed since the last update in ms
 * @return the loss of power during the elapsed time
 */
long getFurnacePowerLoss(long furnacePower, int time);

/**
 * Return the temperature of ambient air with a particular amount of energy
 * @param energy the energy
 * @return the temperature equal to energy/1006 and greater than START_FURNACE_TEMP
 */
int getAmbientTemperature(long energy);

/**
 * The loss of power inside of the cuve for a certain period of time
 * The loss will be of FURNACE_LOSS_OF_POWER_PER_SECOND of the energy
 * @param power the power inside of the cuve
 * @param time the time elapsed since the last update in ms
 * @return the loss of power during the elapsed time
 */
long getWaterPowerLoss(long power, int time);

/**
 * Represents the transfer of energy between the furnace and the water cuve
 * If the water energy is above the furnace energy there is no transfer
 * Else we transfer FURNACE_POWER_TRANSFER of the energy in the furnace to the water cuve
 * @param furnacePower the power inside of the furnace
 * @param time the elapsed time since the last update in ms
 * @return the amount of power transfer from the furnace to the cuve
 */
long getPowerTransfer(long furnacePower, int time);

/**
 * Calculate the temperature of the water based on the quantity of water in l
 * and the power in the water cuve. The formula is the following
 * Energy (J) = Temperature (K) * Mass (kg) * 4.185 (J/kg*K)
 * @param power the power inside the water cuve
 * @param qte the quantity of water in l
 * @return the current temperature of the water
 */
int getWaterTemperatureFromPower(long power, int qte);

/**
 * Calculate the amount of water transformed in steam, the water must reach
 * 100 degree Celsius. So you need that amount of energy
 * Energy (J) = Mass (kg) * 418.5 (J/kg*K)
 * Then the constant change to vaporize water you need that more of enery
 * Energy (J) = Mass (kg) * 2264.76 (J/kg*K)
 * @param power the power inside the water
 * @param qte the amount of water inside the cuve
 * @param steam the amount of steam inside the cuve
 * @param t the time elapsed since the last update in milliseconds
 * @return the amount water that turn in steam
 */
int getSteamAmountProduced(long power, int qte, double steam, int t);

/**
 * Return the energy production based on a certain turbine rotation level
 * You produce Energy (Watt) = RPM * ENERGY_ROTATION_COE * delta (ms)
 * @param turbineRPM the roration of the turbine in %
 * @param delta the time elapsed since the last update in milliseconds
 * @return the amount of Watt produced during the delta interval of time
 */
int getEnergyProduction(int turbineRPM, int delta);

/**
 * Return the rotation of the turbine, with a certain amount of steam getting in
 * the turbine. The rotation during one second will be RPM = QTE * STEAM_ROTATION_COE
 * @param qte the quantity of steam incoming in the turbine
 * @return the rotation of the turbine during that period of time
 */
int getRPMFromQte(double qte);

/**
 * Return the turbine rotation variation
 * @param qte the quantity of steam inputing the turbine
 * @param delta the time elapsed since the last update
 * @param rpm the actual rpm (rpm >= 0 && rpm <= 100)
 * @param the variation in [-rpm;100-rpm]
 */
int getRPMIncreaseFromQte(double qte, int delta, int rpm);

/**
 * The amount of steam that get out of the cuve during a certain period of time
 * We consider that STEAM_OUTPUT_PER_SECOND of the steam get out of the cuve during one second
 * @param steamLevel the amount of steam in the cuve
 * @param time the time elapsed since the last update in ms
 * return the amount of steam to output of the cuve
 */
int getSteamOuput(int steamLevel, int time);

/**
 * The energy needed to transform a certain amount of water into steam
 * Energy (J) = Mass (kg) * 2264.76 (J/kg*K)
 * @param steam the amount of steam we want to calculate the needed energy
 * @return the amount of needed energy to transform steam l of water into steam
 */
int getEnergyForSteam(double steam);

/**
 * Return the turbine rotation needed to produce a certain amount of energy each second
 * @param amount the energy to produce each second in watt
 * @return the turbine rotation needed
 */
int constrant_getRPMFromEnergyProduction(int amount);

/**
 * Return the steam needed to produce a certain rotation of the turbine
 * @param rmp the rotation of the turbine requested
 * @return the steam input needed
 */
int constraint_getSteamAmountFromTurbineRPM(int rpm);

/**
 * Return the needed temperature in the cuve to produce a certain amount of steamLevel
 * inside of the cuve
 * @param steam the amount of steam we want in the cuve
 * @param qte the quantity of water in the cuve
 * @return the temperature needed
 */
int constraint_getTemperatureFromSteam(int steam, int qte);

/**
 * Return the needed temperature in the cuve to produce a certain amount of
 * steam output per second considering that we have a certain quantity of water
 * in the cuve
 * @param steam the amount of output steam needed
 * @param qte the quantity of water we want at all time in the cuve
 * @return the requested energy amount
 */
int constraint_getTemperatureFromSteamOutput(int steam, int qte);

/**
 * Return the coal needed in the furnace to have a certain amount
 * of temeperature inside the water cuve (Energy = 1.006*Temp)
 * @param energy the energy to have in the water cuve
 * @return the coal needed in the furnace
 */
int constraint_getCoalFromFurnaceTemperature(int temp);

/**
 * Return the furnace temperature needed to obtain a certain temperature in the
 * water
 * @param waterlevel the level of water
 * @param temp the water temperature
 * @return the furnace temperature we need
 */
int constraint_getFurnaceTempFromWaterTemp(int waterlevel, int temp);

#endif // PHYSICS_H
