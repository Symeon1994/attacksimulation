/*
   sniffer.c

   Example packet sniffer using the libpcap packet capture library available
   from http://www.tcpdump.org.

   ------------------------------------------

   Copyright © 2012 [Vic Hargrave - http://vichargrave.com]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <slog.h>
#include <libexplain/fgets.h>
#include <signal.h>

#include "modbus_format.h"
#include "flows.h"



pcap_t* pd;
int linkhdrlen;
tuple *hash;
int learning_phase = 0;

pcap_t* open_pcap_socket(char* device, const char* bpfstr)
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* pd;
    uint32_t  srcip, netmask;
    struct bpf_program  bpf;

    // If no network interface (device) is specfied, get the first one.
    if (!*device && !(device = pcap_lookupdev(errbuf)))
    {
        printf("pcap_lookupdev(): %s\n", errbuf);
        return NULL;
    }

    // Open the device for live capture, as opposed to reading a packet
    // capture file.
    if ((pd = pcap_open_live(device, BUFSIZ, 1, 0, errbuf)) == NULL)
    {
        printf("pcap_open_live(): %s\n", errbuf);
        return NULL;
    }

    // Get network device source IP address and netmask.
    if (pcap_lookupnet(device, &srcip, &netmask, errbuf) < 0)
    {
        printf("pcap_lookupnet: %s\n", errbuf);
        return NULL;
    }

    // Convert the packet filter epxression into a packet
    // filter binary.
    if (pcap_compile(pd, &bpf, (char*)bpfstr, 0, netmask))
    {
        printf("pcap_compile(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    // Assign the packet filter to the given libpcap socket.
    if (pcap_setfilter(pd, &bpf) < 0)
    {
        printf("pcap_setfilter(): %s\n", pcap_geterr(pd));
        return NULL;
    }

    return pd;
}

void capture_loop(pcap_t* pd, int packets, pcap_handler func)
{
    int linktype;

    // Determine the datalink layer type.
    if ((linktype = pcap_datalink(pd)) < 0)
    {
        printf("pcap_datalink(): %s\n", pcap_geterr(pd));
        return;
    }

    // Set the datalink layer header size.
    switch (linktype)
    {
    case DLT_NULL:
        linkhdrlen = 4;
        printf("No ethernet header");
        break;

    case DLT_EN10MB:
        linkhdrlen = 14;
        break;

    case DLT_SLIP:
    case DLT_PPP:
        linkhdrlen = 24;
        break;

    default:
        printf("Unsupported datalink (%d)\n", linktype);
        return;
    }

    // Start capturing packets.
    if (pcap_loop(pd, packets, func, 0) < 0)
        printf("pcap_loop failed: %s\n", pcap_geterr(pd));
}

void checkFlow(char* sourceIp,u_int sourcePort,char* destIp,u_int destPort,int proto){
      if(learning_phase){
                tuple* flow;
                int num_item;
                tuple* already = allowed_flow(compute_key(sourceIp,destIp,sourcePort,destPort,proto),hash);
                if(already == NULL){
                    if((flow = create_flow(sourceIp,destIp,sourcePort,destPort,proto))){
                        printf("[Creating flow] (ID:%d Prot:%d) %s:%d->%s:%d\n",
                                flow->id,flow->ipProto,flow->srcIp,flow->srcPort,flow->destIp,flow->destPort);
                        addFlow(flow,&hash);
                        num_item = HASH_COUNT(hash);
                        printf("Number of flows:%u\n",num_item);
                    } else {
                        exit(-1);
                    }
                }
            } else {
                tuple* result = allowed_flow(compute_key(sourceIp,destIp,sourcePort,destPort,proto),hash);
                if(result==NULL){
                    if(proto == IPPROTO_UDP){
                        slog(1,SLOG_NONE,"[%s] [IP/UDP]%s:%d -> %s:%d\n",strclr(CLR_RED,"ALERT"),sourceIp,sourcePort,destIp,destPort);
                    } else {
                        slog(1,SLOG_NONE,"[%s] [IP/TCP]%s:%d -> %s:%d\n",strclr(CLR_RED,"ALERT"),sourceIp,sourcePort,destIp,destPort);
                    }
                } else {
                    if(proto == IPPROTO_UDP){
                        slog(0,SLOG_NONE,"[%s] [IP/UDP]%s:%d -> %s:%d\n",strclr(CLR_GREEN,"ALLOW"),sourceIp,sourcePort,destIp,destPort);
                    } else {
                        slog(0,SLOG_NONE,"[%s] [IP/TCP]%s:%d -> %s:%d\n",strclr(CLR_GREEN,"ALLOW"),sourceIp,sourcePort,destIp,destPort);
                    }
                }
            }
}

void packetHandler(u_char *userData, const struct pcap_pkthdr* pkthdr, const u_char* packet) {
    struct ether_header* ethernetHeader;
    struct ip* ipHeader;
    struct tcphdr* tcpHeader;
    struct udphdr* udphdr;
    struct icmphdr* icmphdr;
    char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];
    u_int sourcePort, destPort;
    //u_char *data;
    //modbus_packet* payload;
    //modbus_res* response;
    //int dataLength = 0;
    unsigned short id, seq;

    ethernetHeader = (struct ether_header*)packet;
    if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) {
        ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
        inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIp, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->ip_dst), destIp, INET_ADDRSTRLEN);

        if (ipHeader->ip_p == IPPROTO_TCP) {
            tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
            sourcePort = ntohs(tcpHeader->source);
            destPort = ntohs(tcpHeader->dest);
            checkFlow(sourceIp,sourcePort,destIp,destPort,IPPROTO_TCP);
            //data = (u_char*)(packet + sizeof(struct ether_header) + sizeof(struct ip) + 4*tcpHeader->doff);
            //dataLength = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + 4*tcpHeader->doff);
        }

        else if(ipHeader->ip_p == IPPROTO_UDP){
            udphdr = (struct udphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
            sourcePort = ntohs(udphdr->source);
            destPort = ntohs(udphdr->dest);
            checkFlow(sourceIp,sourcePort,destIp,destPort,IPPROTO_UDP);
            //TODO check time with timer



        } else if(ipHeader->ip_p == IPPROTO_ICMP){
            icmphdr = (struct icmphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));
            memcpy(&id, (u_char*)icmphdr+4, 2);
            memcpy(&seq, (u_char*)icmphdr+6, 2);
            slog(0,SLOG_NONE,"[%s] [ICMP] %s -> %s\tType:%d Code:%d ID:%d Seq:%d\n",
                    strclr(CLR_GREEN,"ALLOW"),
                    sourceIp,destIp, icmphdr->type,
                    icmphdr->code,ntohs(id),ntohs(seq));
        }
    }
}


int create_hash(FILE* file){
    char line[256];
    tuple* flow=NULL;
    u_int destPort;
    u_int srcPort;
    u_int8_t ipProto;
    char* dest;
    char* src;
    int num_item;
    while(fgets(line,sizeof(line),file)!=NULL){
        parse_line(line,&src,&dest,&srcPort,&destPort,&ipProto);
        if((flow = create_flow(src,dest,srcPort,destPort,ipProto))){
            printf("[Creating flow] (ID:%d Prot:%d) %s:%d->%s:%d\n",flow->id,flow->ipProto,flow->srcIp,flow->srcPort,flow->destIp,flow->destPort);
            addFlow(flow,&hash);
            num_item = HASH_COUNT(hash);
            printf("Number of flows:%u\n",num_item);
        } else {
             return EXIT_FAILURE;
        }
    }
    return EXIT_SUCCESS;
}

void bailout(int signo)
{
    struct pcap_stat stats;

    if (pcap_stats(pd, &stats) >= 0)
    {
        printf("%d packets received\n", stats.ps_recv);
        printf("%d packets dropped\n\n", stats.ps_drop);
    }
    clearHash(&hash);
    pcap_close(pd);
    exit(0);
}

void finish_learning(int signo){
     printf("\nFinished learning phase\n");
     learning_phase = 0;
}

int main(int argc, char **argv)
{
    char interface[256] = "", bpfstr[256] = "";
    int packets = 0, c, i;
    unsigned int time = 0;

    char* filename = NULL;
    // Get the command line options, if any
    while ((c = getopt (argc, argv, "hi:n:f:t:")) != -1)
    {
        switch (c)
        {
        case 'h':
            printf("usage: %s [-h] [-i ] [-f] [-n ] \n", argv[0]);
            exit(0);
            break;
        case 'i':
            strcpy(interface, optarg);
            break;
        case 'n':
            packets = atoi(optarg);
            break;
        case 'f':
            filename = optarg;
            break;
        case 't':
            time = atoi(optarg);
            learning_phase = 1;
        }
    }

    if(filename != NULL){
        FILE* file = fopen(filename,"r+");
        if(file == NULL){
             printf("Could not open file\n");
             return EXIT_FAILURE;
        }
        printf("Creating Rules...\n");
        if(create_hash(file)){
            printf("Could not create rules\n");
            return EXIT_FAILURE;
        }
        fclose(file);
    }

    if(time !=0){
        signal(SIGALRM,finish_learning);
        alarm(time);
    }

    /*
        slog_init - Initialise slog
        First argument is log filename
        Second argument is config file
        Third argument is max log level on console
        Fourth is max log level on file
        Fitfh is thread safe flag
    */
    slog_init("logs/trace_log.txt","log.cfg",2,2,1);


    // Get the packet capture filter expression, if any.
    for (i = optind; i < argc; i++)
    {
        strcat(bpfstr, argv[i]);
        strcat(bpfstr, " ");
    }

    // Open libpcap, set the program termination signals then start
    // processing packets.
    if ((pd = open_pcap_socket(interface, bpfstr)))
    {
        signal(SIGINT, bailout);
        signal(SIGTERM, bailout);
        signal(SIGQUIT, bailout);
        capture_loop(pd, packets, (pcap_handler)packetHandler);
        bailout(0);
    }
    exit(0);
}
