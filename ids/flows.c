/*#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uthash.h"

#define ANY "any"
#define ANYPORT 0

typedef struct flow_tup tuple;

struct flow_tup{
    int id;              //field as the key
    UT_hash_handle hh;   //make this strucure hashtable
    char    *srcIp;
    char    *destIp;
    u_int   srcPort;
    u_int   destPort;
};*/

#include "flows.h"
#define ETHER_TYPE_IP (0x0800)
#define ETHER_TYPE_8201Q (0x8100)

int compute_key(char *srcIp,char *destIP,u_int srcPort,u_int destPort,u_int8_t ipProto){
    return ((size_t)(*srcIp)*59) ^
        ((size_t)(*destIP))^
        ((size_t)(srcPort)<<16)^
        ((size_t)(destPort))^
        ((size_t)(ipProto));
}

tuple* create_flow(char *srcIp,char *destIp,u_int srcPort,u_int destPort,u_int8_t ipProto){

    tuple* flow;
    int flow_id = compute_key(srcIp,destIp,srcPort,destPort,ipProto);
    flow =(tuple*) malloc(sizeof(tuple));
    if(flow!=NULL){
        flow->id = flow_id;
        flow->srcIp = (char*) malloc(16*sizeof(char));
        flow->destIp = (char *)malloc(16*sizeof(char));
        if(flow->srcIp || flow->destIp){
             flow->srcIp = srcIp;
             flow->destIp = destIp;
        } else {
            printf("[create_flow]-Could not allocate memory for flow");
            free(flow);
            flow=NULL;
            return NULL;
        }
        flow->destIp = destIp;
        flow->srcPort = srcPort;
        flow->destPort = destPort;
        flow->ipProto = ipProto;
        return flow;
    } else {
        printf("[create_flow]-Could not allocate memory for flow");
        return NULL;
    }
}


void parse_line(char line[],char **srcIp,char **destIp,u_int *srcPort,u_int *destPort,u_int8_t *ipProto){
    char *word;
    word=strtok(line,"\t");
    //1st word
    *srcIp = word;
    word = strtok(NULL,"\t");
    //2nd word
    if(strncmp(word,ANY,3)==0){
        *srcPort=ANYPORT;
    } else{
        *srcPort = atoi(word);
    }
    word = strtok(NULL,"\t");
    //3rd word
    *destIp = word;
    word = strtok(NULL,"\t");
    //Last word
    if(strncmp(word,ANY,3)==0){
        *destPort=ANYPORT;
    } else {
        *destPort = atoi(word);
    }
    word = strtok(NULL,"\t");
    *ipProto = atoi(word);
}

void addFlow(tuple *flow,tuple **hash_f){
    HASH_ADD_INT(*hash_f,id,flow);
}

void clearHash(tuple** hash_f){
     tuple* current,*tmp;
     HASH_ITER(hh,*hash_f,current,tmp){
          HASH_DEL(*hash_f,current);
          free(current);
     }
}

tuple *allowed_flow(int flow_id,tuple* hash_f){
    tuple *flow;
    HASH_FIND_INT(hash_f,&flow_id,flow);
    return flow;
}

void remove_flow(tuple* flow,tuple** hash_f){
     HASH_DEL(*hash_f,flow);
     free(flow);
}

