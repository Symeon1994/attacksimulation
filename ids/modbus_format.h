
typedef struct modbus_query{
    uint16_t    trans_id;
    uint16_t    prot_id;
    uint16_t    msg_len;
    uint8_t     unit_id;
    uint8_t     func_code;
    uint16_t    addr_reg;
    uint16_t    nbr_reg;
} modbus_packet;

typedef struct modbus_response{
    uint16_t    trans_id;
    uint16_t    prot_id;
    uint16_t    msg_len;
    uint8_t     unit_id;
    uint8_t     func_code;
    uint8_t     byte_count;
    uint16_t    reg0;
    uint16_t    reg1;
    uint16_t    reg2;
    uint16_t    reg3;
    uint16_t    reg4;
    uint16_t    reg5;
    uint16_t    reg6;
    uint16_t    reg7;
    uint16_t    reg8;
    uint16_t    reg9;
    uint16_t    reg10;
    uint16_t    reg11;
    uint16_t    reg12;
    uint16_t    reg13;
    uint16_t    reg14;
    uint16_t    reg15;
} modbus_res;

