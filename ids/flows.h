#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <net/ethernet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pcap.h>
#include "uthash.h"

#define ANY "any"
#define ANYPORT 0

typedef struct flow_tup tuple;

struct flow_tup{
    int id;              //field as the key
    UT_hash_handle hh;   //make this strucure hashtable
    char    *srcIp;
    char    *destIp;
    u_int   srcPort;
    u_int   destPort;
    u_int8_t    ipProto;
};

int compute_key(char *srcIp,char *destIP,u_int srcPort,u_int destPort,u_int8_t ipProto);
tuple* create_flow(char *srcIp, char *destIp,u_int srcPort,u_int destPort,u_int8_t ipProto);
void parse_line(char line[],char **srcIp,char **destIp,u_int *srcPort,u_int *destPort,u_int8_t *ipProto);
void addFlow(tuple *flow,tuple **hash_f);
tuple* allowed_flow(int flow_id,tuple* hash_f);
void remove_flow(tuple* flow,tuple** hash_f);
void clearHash(tuple** hash_f);
