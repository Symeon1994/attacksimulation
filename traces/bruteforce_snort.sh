#!/bin/usr/sh

# Clean
rm snort_bruteforce
rm snort_bruteforce_id
rm snort_bruteforce_result

# Test
sudo snort -r bruteforce_attack.pcap -c /etc/snort/snort.conf -A console:test -k none > snort_bruteforce
python ../src/CLI/utils/get_index.py snort_bruteforce snort_bruteforce_id
python ../src/CLI/utils/analyze.py bruteforce_attack.pcap snort_bruteforce_id bruteforce_attacker >> snort_bruteforce_result


rm snort_bruteforce
rm snort_bruteforce_id
