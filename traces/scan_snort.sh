#!/bin/usr/sh

# Clean
rm snort_scan
rm snort_scan_id
rm snort_scan_result

# Test
sudo snort -r scan_attack.pcap -c /etc/snort/snort.conf -A console:test -k none > snort_scan
python ../src/CLI/utils/get_index.py snort_scan snort_scan_id
python ../src/CLI/utils/analyze.py scan_attack.pcap snort_scan_id scan_attacker >> snort_scan_result


rm snort_scan
rm snort_scan_id
