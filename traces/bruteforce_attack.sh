#!/bin/usr/sh
rm -r -f bruteforce_attack
mkdir bruteforce_attack
rm -r -f bruteforce_id 
mkdir bruteforce_id
rm -f bruteforce_result
# Generate training
for i in {10..100..10} 
do
  python ../src/CLI/utils/truncate.py bruteforce_attack.pcap "bruteforce_attack/bruteforce_attack$i.pcap" $i
  ../src/ids/sniffer -f "bruteforce_attack/bruteforce_attack$i.pcap" -m bruteforce_attack.pcap -p 502 -s 0 -o "bruteforce_id/bruteforce_attack$i"
  echo $i >> bruteforce_result
  python ../src/CLI/utils/analyze.py bruteforce_attack.pcap "bruteforce_id/bruteforce_attack$i" bruteforce_attacker >> bruteforce_result
done