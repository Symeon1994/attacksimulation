#!/bin/usr/sh

echo "Do not forget to change the CONTROL_PORTS in the snort.conf file!"

# Clean
rm snort_dos
rm snort_dos_id
rm snort_dos_result

# Test
sudo snort -r dos_attack.pcap -c /etc/snort/snort.conf -A console:test -k none > snort_dos
python ../src/CLI/utils/get_index.py snort_dos snort_dos_id
python ../src/CLI/utils/analyze.py dos_attack.pcap snort_dos_id dos_attacker >> snort_dos_result


rm snort_dos
rm snort_dos_id
