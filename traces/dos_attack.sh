#!/bin/usr/sh
rm -r -f dos_attack
mkdir dos_attack
rm -r -f dos_id 
mkdir dos_id
rm -f dos_result
# Generate training
for i in {10..100..10} 
do
  python ../src/CLI/utils/truncate.py dos_attack.pcap "dos_attack/dos_attack$i.pcap" $i
  ../src/ids/sniffer -f "dos_attack/dos_attack$i.pcap" -m dos_attack.pcap -p 502 -s 0 -o "dos_id/dos_attack$i"
  echo $i >> dos_result
  python ../src/CLI/utils/analyze.py dos_attack.pcap "dos_id/dos_attack$i" dos_attacker >> dos_result
done