#!/bin/usr/sh
rm -r -f scan_attack
mkdir scan_attack
rm -r -f scan_id 
mkdir scan_id
rm -f scan_result
# Generate training
for i in {10..55..5} 
do
  python ../src/CLI/utils/truncate.py scan_attack.pcap "scan_attack/scan_attack$i.pcap" $i
  ../src/ids/sniffer -f "scan_attack/scan_attack$i.pcap" -m scan_attack.pcap -p 502 -s 0 -o "scan_id/scan_attack$i"
  echo $i >> scan_result
  python ../src/CLI/utils/analyze.py scan_attack.pcap "scan_id/scan_attack$i" scan_attacker >> scan_result
done
rm -r -f scan_attack
rm -r -f scan_id